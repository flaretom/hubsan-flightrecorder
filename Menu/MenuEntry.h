/*
 * MenuEntry.h
 *
 *  Created on: 16.01.2020
 *      Author: tom
 */

#ifndef MENUENTRY_H_
#define MENUENTRY_H_

struct tagMenuEntry_t
{
    static const int MaxEntries = 10;
    char title[16];
    const char* (*command)(const tagMenuEntry_t * const currEntry, int key);
    tagMenuEntry_t *submenu[MaxEntries];
} ;

using MenuEntry_t = tagMenuEntry_t;

#endif /* MENUENTRY_H_ */
