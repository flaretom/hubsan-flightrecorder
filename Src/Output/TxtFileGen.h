/*
 * TxtFileGen.h
 *
 *  Created on: 16.10.2019
 *      Author: tom
 */

#ifndef TXTFILEGEN_H_
#define TXTFILEGEN_H_

#include "IFileGen.h"
#include <ff.h>

class TxtFileGen : public IFileGen {
	FIL outfile;
	bool isEnabled = false;
public:
	TxtFileGen();
	virtual ~TxtFileGen();
	bool open(const char *date, const char* time);
	void close();
	bool createEntry(const FlightDataContainer &FDC);
};

#endif /* TXTFILEGEN_H_ */
