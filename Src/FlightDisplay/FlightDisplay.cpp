/*
 * Flightm_pDisplay->cpp
 *
 *  Created on: 29.02.2020
 *      Author: tom
 */

#include <FlightDisplay/FlightDisplay.h>
#include <Menu/IDisplayBase.h>
#include <FlightData.h>
#include <FlRecMenu/HubsanDisplay.h>
#include <FlRecConfig/FlRecConfig.h>

#include <ctype.h>
#include <string.h>
#include <cstdio>
#include <math.h>

FlightDisplay::FlightDisplay(IDisplayBase *pDisplay) {
	// TODO Auto-generated constructor stub
	m_pDisplay = pDisplay;
}

const char *FlightDisplay::findEndOfField(const char *pStr)
{
	const char *p = pStr;
	while(*p && isdigit(*p)) {
		++p;
	}
	return p;
}

void FlightDisplay::ParseDisplayConfig(const char *CfgStr)
{
	uint8_t col = 0U;
	uint8_t row = 0U;
	uint8_t format = 0U;
	char Field[128];
	uint8_t FIdx = 0;
	const char *Cfg = CfgStr;
	Field[0] = 0;
	while (*Cfg) {
		switch(*Cfg) {
			case '$': // dataitem # follows
				{
					++Cfg;
					uint8_t field = atoi(Cfg);
					//strcat(Field,FillDisplayField(field));
					FIdx += FDC.asString(static_cast<FlightDataIds>(field), &Field[FIdx], sizeof(Field) - FIdx);
					Cfg = findEndOfField(Cfg);
					break;
				}
			case '@': // position follows
				if (Field[0]) {
//					m_pDisplay->printFixed( col, row, Field);
					ssd1306_printFixedN (col*32,  row*8,  Field, STYLE_NORMAL, format);
					Field[FIdx] = 0;
					FIdx = 0;
				}
				++Cfg;
				row = atoi(Cfg);
				Cfg = findEndOfField(Cfg);
				++Cfg;
				col = atoi(Cfg);
				Cfg = findEndOfField(Cfg);
				break;
			case '&': // format switch follows
				++Cfg;
				format = atoi(Cfg);
				Cfg = findEndOfField(Cfg);
				break;
			default:
				Field[FIdx] = *Cfg;
				Field[++FIdx] = 0;
				++Cfg;
				break;
		}
	}
	if (Field[0]) {
		//m_pDisplay->printFixed( col, row, Field);
		ssd1306_printFixedN (col*32,  row*8,  Field, STYLE_NORMAL, format);
	}
}

const uint8_t g_customFont_5x16[] =
{
    0x00,  // 0x00 means fixed font type - the only supported by the library
    0x05,  // 0x05 = 5 - font width in pixels
    0x10,  // 0x10 = 16 - font height in pixels
    0x30,  // 0x30 = 48 - first ascii character number in the font ('0' = ascii code 48)
    // '0'
    0b00000000,  // upper 8 pixels of character
    0b11111100,
    0b00000011,
    0b00000011,
    0b11111100,

    0b00000000,  // lower 8 pixels of character
    0b00011111,
    0b01100000,
    0b01100000,
    0b00011111,
    // '1'
    0b00000000,  // upper 8 pixels of character
    0b00001100,
    0b11111111,
    0b00000000,
    0b00000000,

    0b00000000,  // lower 8 pixels of character
    0b01100000,
    0b01111111,
    0b01100000,
    0b00000000,

    0x00, // End of font
};

const char *bit_rep[16] = {
    [ 0] = "0000", [ 1] = "0001", [ 2] = "0010", [ 3] = "0011",
    [ 4] = "0100", [ 5] = "0101", [ 6] = "0110", [ 7] = "0111",
    [ 8] = "1000", [ 9] = "1001", [10] = "1010", [11] = "1011",
    [12] = "1100", [13] = "1101", [14] = "1110", [15] = "1111",
};

bool FlightDisplay::update()
{
//	m_pDisplay->clear();
	for (uint8_t i = 0; ; ++i) {
		const char *pDispStr = GlobalSettings.getDisplayFieldConfig(i);
		if (nullptr == pDispStr) {
			break;
		}
		ParseDisplayConfig(pDispStr);
	}
//	m_pDisplay->update();
//	char szBuffer[12];
//	static uint8_t i = 0;
//	if (0==i) {
//		ssd1306_fillScreen(0x00);
//	}
//    sprintf(szBuffer, "%s", bit_rep[i >> 4]); //, bit_rep[i & 0x0F]);
//    ssd1306_setFixedFont( ssd1306xled_font6x8 );
//    ssd1306_printFixed (0,  0,  szBuffer, STYLE_NORMAL );
//    ssd1306_printFixedN (0, 16, szBuffer, STYLE_NORMAL, FONT_SIZE_2X);
//    ssd1306_printFixedN (0, 32, szBuffer, STYLE_NORMAL, FONT_SIZE_4X);
//    ++i;
	return true;
}

