/*
 * DataStorage.h
 *
 *  Created on: 17.01.2020
 *      Author: tom
 */

#ifndef IDATASTORAGE_H_
#define IDATASTORAGE_H_

class IDataStorage {
public:
    virtual int32_t getData(const MenuEntry_t * const MenuPt) = 0;
    virtual bool setData(const MenuEntry_t * const MenuPt, int32_t value) = 0;
};

#endif /* IDATASTORAGE_H_ */
