
#ifndef BasicTimer_H
#define BasicTimer_H

#include <stdint.h>
#include <limits.h>

typedef uint32_t TIME_T;
const TIME_T TIMEMAX_T=UINT_MAX;
TIME_T GetSysTime();

#ifndef boolean_t
typedef bool boolean_t;
#endif 

class BasicTimer {
   
public :

    BasicTimer();
    
    ////    Operations    ////
    
    boolean_t IsActive() const;
    
    inline void Deactivate() { m_EndTime = 0U; }

    //## operation Remaining() const
    TIME_T Remaining() const;
    
    // Start a peridic timeout. After reaching the endtime (now+interval) the Timeout-method will return TRUE. The Timeout call will return once a TRUE, and only after a new period is ended again TRUE. See Timeout activity/sequence diagram for details.
    //## operation SetPeriodicTimeout(u32)
    void SetPeriodicTimeout(const TIME_T Interval);
    
    // Start a single-shot timeout. After reaching the endtime (now+interval) the Timeout-method will return TRUE.
    //## operation SetSingleTimeout(u32)
    void SetSingleTimeout(const TIME_T Interval);
    
    //## operation Timedout()
    boolean_t Timedout();
    
    static void Delay(TIME_T delay_ms);
    ////    Attributes    ////

    // time past since a given timestamp until current time (takes wrap around into account)
    static TIME_T PastTime(uint32_t timestamp);

protected :

    // Attribute holds the endtime-timestamp. 
    TIME_T m_EndTime;		//## attribute m_EndTime_u32
    
    // Stores the intervall for periodic timers. This value will be added after each timeout to get the next endtime.
    TIME_T m_Interval;		//## attribute m_Interval_u32
};

class Timestamp
{
public:
	void Set();
    TIME_T TimePasted(bool Update = false);
protected:
    TIME_T m_Timestamp;		//## attribute m_EndTime_u32
};

#endif
