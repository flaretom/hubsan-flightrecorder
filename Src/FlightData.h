/*
 * CppMain.h
 *
 *  Created on: 16.10.2019
 *      Author: tom
 */

#ifndef FLIGHTDATA_H_
#define FLIGHTDATA_H_

#include <stdint.h>
#include <Utilities/BasicTimer.h>

typedef enum {
	cNavData = 0x01,
	cTeleData = 0x02,
	cControl = 0x04
} FrameType_t;


typedef struct {
	uint8_t validFrames = 0U;
	// data from 0xE8
	uint8_t Aux1 = 0U;
	uint8_t Aux2 = 0U;
	uint32_t latit = 0U;
	uint32_t longit = 0U;
	int16_t elevat = 0U;
	uint16_t distance = 0U;
	// data from 0xE7
	uint8_t VBat = 0U;
	uint8_t Sat = 0U;
	int16_t H = 0U;
	int16_t R = 0U;
	int16_t P = 0U;
	// data from control
	int8_t throttle = 0U;
	int8_t rudder = 0U;
	int8_t pitch = 0U;
	int8_t yaw = 0U;
	uint8_t marker = 0U;
	uint8_t RSSI = 0U;
	uint8_t velocity  = 0U;
	bool video =  false;
	bool photoTgl = false;
	// calc data
	uint32_t RecordStart = 0U;
	uint8_t vMax  = 0U;
} FlightData_t;

typedef enum {
	eDID_NONE 		 = 0,
	eDID_TIME        = 1,
	eDID_DATE        = 2,
	eDID_LAT         = 3,
	eDID_LONG        = 4,
	eDID_ELEVAT      = 5,
	eDID_DISTANCE    = 6,
	eDID_VBAT        = 7,
	eDID_SAT         = 8,
	eDID_H           = 9,
	eDID_R           = 10,
	eDID_P           = 11,
	eDID_THROTTLE    = 12,
	eDID_RUDDER      = 13,
	eDID_PITCH       = 14,
	eDID_YAW         = 15,
	eDID_MARKER      = 16,
	eDID_VIDEO       = 17,
	eDID_PHOTOTGL    = 18,
	eDID_AUX1        = 19,
	eDID_AUX2        = 20,
	eDID_RECORDTIME  = 21,
	eDID_BATPERCENT  = 22,
	eDID_VELOCITY    = 23,
	eDID_RSSI        = 24,
	eDID_VALIDFRAMES = 25,
	eDID_GPS_MODE	 = 26,
	eDID_GPS_RTH	 = 27,
	eDID_MODE_I	 	 = 28,
	eDID_MODE_T	 	 = 29,
	eDID_VMAX		 = 30
} FlightDataIds;

class FlightDataContainer
{
	FlightData_t FD;
	BasicTimer StartDetectTimer;
	BasicTimer RepeatTimer[4U];

	char m_TimeStr[20];
	char m_DateStr[20];

	bool emulateButton(bool pushed, BasicTimer &timer);
	uint8_t EstimateBatCapacity(const uint8_t Vbat) const;

public:
	inline const FlightData_t *get() const { return &FD; }
	inline FlightData_t *update() { return &FD; }
	inline void updateMarker(const uint8_t newMarker) { FD.marker |= newMarker;}
	inline void clearMarker() { FD.marker = 0U;}
	inline void clearFrames() { FD.validFrames = 0U;}
	uint32_t getU(const FlightDataIds item, bool &ok) const;
	int32_t getI(const FlightDataIds item, bool &ok) const;
	uint8_t asString(const FlightDataIds item, char *szBuffer, const uint16_t bufsize) const;
	const char * asString(const FlightDataIds item, uint8_t &length, const uint16_t bufsize = 0U) const;

	void HandleReceivedFrameE8(const uint8_t *framedata);
	void HandleReceivedFrameE7(const uint8_t *framedata);
	void HandleReceivedFrame05(const uint8_t *framedata);
	void InjectTestData(bool doInit);
	const char* getFormat(const FlightDataIds item) const;
	void startRecording();
	void updateTimeDate();

	bool isStartStopCondition();
	bool Down();
	bool Up();
	bool Right();
	bool Left();

	inline bool writeRAWEntry(const uint8_t frameType) const { return (0!=(FD.validFrames & frameType)) || (0U != FD.marker);}
};

extern FlightDataContainer FDC;

#endif /* FLIGHTDATA_H_ */
