/*
 * GpxFileGen.cpp
 *
 *  Created on: 16.10.2019
 *      Author: tom
 */

#include "GpxFileGen.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

const char *GpxFileGen::GpxfileHeader="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" \
		"<gpx version=\"1.0\" >\n";

const char *GpxFileGen::GpxTrackname = "<trk><name>%s</name>\n<trkseg>\n";
const char *GpxFileGen::GpxTrkpoint1 = "<trkpt lat=\"%s\"";
const char *GpxFileGen::GpxTrkpoint2 = " lon=\"%s\">";
const char *GpxFileGen::GpxTrkpoint3 = "<ele>%s</ele>";
const char *GpxFileGen::GpxTrkpoint4 = "<time>%sT";
const char *GpxFileGen::GpxTrkpoint5 = "%sZ</time>";
const char *GpxFileGen::GpxTrkpointEnd = "</trkpt>\n";
const char *GpxFileGen::GpxTrkpointInvalid = "<Invalid/>";
const char *GpxFileGen::GpxfileFooter = "</trkseg>\n</trk>\n</gpx>\n";

GpxFileGen::GpxFileGen() {
	// TODO Auto-generated constructor stub

}

GpxFileGen::~GpxFileGen() {
	// TODO Auto-generated destructor stub
}

bool GpxFileGen::open(const char *date, const char* time)
{
	if (!isEnabled) {
		FRESULT fresult;
		UINT lenwrt;
		char buffer[80];
		sprintf(buffer, "H501_%s_%s.GPX", date, time);
		fresult = f_open(&outfile, buffer, FA_OPEN_ALWAYS | FA_WRITE); //open file on SD card
		if (FR_OK == fresult) {
			fresult = f_lseek(&outfile, outfile.fsize);         //go to the end of the file
			fresult = f_write(&outfile, GpxfileHeader, strlen(GpxfileHeader), &lenwrt);
			uint16_t Idx = snprintf(buffer, sizeof(buffer), GpxTrackname, "H501SS");
			fresult = f_write(&outfile, buffer, Idx, &lenwrt);
			f_sync(&outfile);
		}
		isEnabled = FR_OK == fresult;
	}
	return isEnabled;
}

void GpxFileGen::close()
{
	if (isEnabled) {
		UINT lenwrt;
		f_write(&outfile, GpxfileFooter, strlen(GpxfileFooter), &lenwrt);
		f_sync(&outfile);
		f_close(&outfile);
	}
	isEnabled = false;
}

bool GpxFileGen::createEntry(const FlightDataContainer &FDC)
{
	const auto bufsize = 256U;
	char buf[bufsize];
	bool res = false;
	uint16_t idx = 0U;
	if (isEnabled) {
		uint8_t length;
		bool ok;
		idx += snprintf(buf + idx , bufsize - idx, GpxTrkpoint1, FDC.asString(eDID_LAT, length));
		idx += snprintf(buf + idx , bufsize - idx, GpxTrkpoint2, FDC.asString(eDID_LONG, length));
		idx += snprintf(buf + idx , bufsize - idx, GpxTrkpoint3, FDC.asString(eDID_ELEVAT, length));
		idx += snprintf(buf + idx , bufsize - idx, GpxTrkpoint4, FDC.asString(eDID_DATE, length));
		idx += snprintf(buf + idx , bufsize - idx, GpxTrkpoint5, FDC.asString(eDID_TIME, length));
		if ( (0 == FDC.getU(eDID_LAT, ok)) || (FDC.getU(eDID_LAT, ok) > 3000000000)) {
			idx += snprintf(buf + idx , bufsize - idx, GpxTrkpointInvalid);
		}
		idx += snprintf(buf + idx , bufsize - idx, GpxTrkpointEnd);
		res = true;
	}
	if (res) {
		UINT lenwrt;
		f_write(&outfile, buf, idx, &lenwrt);
		f_sync(&outfile);
	}
	return res;
}


