﻿/*
DebouncedSwitch.cpp

http://www.avdweb.nl/arduino/hardware-interfacing/simple-switch-debouncer.html

Copyright (C) 2012  Albert van Dalen http://www.avdweb.nl
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License at http://www.gnu.org/licenses .
 
Version 20-4-2013
_debouncePeriod=50
Version 22-5-2013
Added longPress, doubleClick
Version 1-12-2015
added process(input)
Version 15-1-2016
added deglitching



    pushed()
    Use only for push buttons. It returns "true" if a button was pushed after the poll() instruction was executed. Note that this is in fact "single-click".
    released()
    It returns "true" if a push button was released, this will however rarely be used.
    on()
    Use only for toggle switches. It returns "true" as long as the switch is in the "on" position. The polarity of the switch in the "on" position has to be filled in correctly. There is no off() function, this is simply !on().
    longPress()
    It returns "true" if a push button is pressed longer than 300ms. The note at doubleClick() applies also here.
    doubleClick()
    It returns "true" if a push button is double clicked within 250ms. Note that a doubleClick() always will be preceded by pushed() from the first push. This can't be avoided, because then the pushed() function would have to wait on a possible second click, which would introduce an annoying delay. So, the action on doubleClick() has to undone the previous action on pushed().

 
..........................................DEGLITCHING..............................
                                           
                        ________________   _
               on      |                | | |    _                        
                       |                | | |   | |                                      
                       |                |_| |___| |__                                                           
 analog        off_____|_____________________________|____________________________  
                   
                        ________________   _     _
 input            _____|                |_| |___| |_______________________________               
            
 poll            ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^   
 
 equal           0 1 1 0 1 1 1 1 1 1 1 1 0 0 0 1 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
 
 deglitchPeriod          <--------><--   <--     <-  <--------><--------><--------
                                    ___________________________
 deglitched       _________________|                           |__________________
 
 deglitchTime            ^         ^     ^       ^   ^         ^        ^
 
 ..........................................DEBOUNCING.............................
 
 debouncePeriod                    <-------------------------------->    
                                    _________________________________
 debounced        _________________|                                 |____________  
                                    _                                 _
 _switched        _________________| |_______________________________| |__________       
                                                     
 switchedTime                      ^                                 ^ 
  
 
**********************************************************************************
........................................DOUBLE CLICK..............................
                                         
                           __________         ______                           
 debounced        ________|          |_______|      |_____________________________  
 
 poll            ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^          
                           _                  _
 pushed          _________| |________________| |__________________________________       
                                                     
 pushedTime               ^                  ^ 
 
 doubleClickPeriod         <------------------------------------->                     
                                              _
 _doubleClick     ___________________________| |__________________________________
 
                             
........................................LONG PRESS................................
                                          
                           ___________________________                                     
 debounced        ________|                           |___________________________        
 
 poll            ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^             
         
 longPressPeriod            <--------------->         
                            _                           _
 _switched        _________| |_________________________| |________________________       
                                              __________
 longPressDisable ___________________________|          |_________________________                                  
                                              _
 _longPress       ___________________________| |__________________________________        
  
*/
 
//#include "Arduino.h"
#include <stdint.h>
#include <ctime>
#include "DebouncedSwitch.h"

time_t GetSysTime();

bool digitalRead(uint8_t pin);
void pinMode(uint8_t pin, uint8_t mode);

               
DebouncedSwitch::DebouncedSwitch(uint8_t _pin, uint8_t PinMode, bool polarity, int debouncePeriod, int longPressPeriod, int doubleClickPeriod, int deglitchPeriod):
pin(_pin), deglitchPeriod(deglitchPeriod), debouncePeriod(debouncePeriod), longPressPeriod(longPressPeriod), doubleClickPeriod(doubleClickPeriod), polarity(polarity)
{ pinMode(pin, PinMode);
  switchedTime = GetSysTime();
  debounced = digitalRead(pin);
}

void DebouncedSwitch::init()
{
	input = digitalRead(pin);
	deglitched = input;
	debounced = deglitched;
	_switched = 0;
}
  
bool DebouncedSwitch::poll()
{
	input = digitalRead(pin);
	return process();
}
 
bool DebouncedSwitch::process()
{
	deglitch();
	debounce();
	calcDoubleClick();
	calcLongPress();
	return _switched;
}
 
void inline DebouncedSwitch::deglitch()
{
	ms = GetSysTime();
	if(input == lastInput) {
		equal = 1;
	}
	else {
		equal = 0;
		deglitchTime = ms;
	}
	if(equal && ((ms - deglitchTime) > deglitchPeriod)) { // max 50ms, disable deglitch: 0ms
		deglitched = input;
		deglitchTime = ms;
	}
	lastInput = input;
}
 
void inline DebouncedSwitch::debounce()
{
	ms = GetSysTime();
	_switched = 0;
	if((deglitched != debounced) && ((ms - switchedTime) >= debouncePeriod)) {
		switchedTime = ms;
		debounced = deglitched;
		_switched = 1;
		longPressDisable = false;
	}
}
 
void inline DebouncedSwitch::calcDoubleClick()
{
	_doubleClick = false;
	if(pushed())  {
		_doubleClick = (ms - pushedTime) < doubleClickPeriod; // pushedTime of previous push
		pushedTime = ms;
	}
}
 
void inline DebouncedSwitch::calcLongPress()
{
	_longPress = false;
	if(!longPressDisable)  {
		_longPress = on() && ((ms - pushedTime) > longPressPeriod); // true just one time between polls
		longPressDisable = _longPress; // will be reset at next switch
	}
}
 
bool DebouncedSwitch::switched()
{
	return _switched;
}
 
bool DebouncedSwitch::on()
{
	return !(debounced^polarity);
}
 
bool DebouncedSwitch::pushed()
{
	return _switched && !(debounced^polarity);
}
 
bool DebouncedSwitch::released()
{
	return _switched && (debounced^polarity);
}
 
bool DebouncedSwitch::longPress()
{
	return _longPress;
}
 
bool DebouncedSwitch::doubleClick()
{
	return _doubleClick;
}
