/*
    MIT License

    Copyright (c) 2018, Alexey Dynda

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include "ssd1306_i2c_twi.h"
#include "intf/ssd1306_interface.h"
#include "lcd/ssd1306_commands.h"
#include "ssd1306_i2c.h"

#if defined(CONFIG_STM32_I2C_ENABLE)

#include "i2c.h"
#include "stm32f1xx_hal.h"

#include <stdbool.h>

/* Max i2c frequency, supported by OLED controllers */
#define SSD1306_TWI_FREQ  400000
#define MAX_RETRIES       64

static uint8_t s_sa = 0;
static uint8_t I2C_error = 0;

/* Private define for @ref PreviousState usage */
#define I2C_STATE_MSK             ((uint32_t)((uint32_t)((uint32_t)HAL_I2C_STATE_BUSY_TX | (uint32_t)HAL_I2C_STATE_BUSY_RX) & (uint32_t)(~((uint32_t)HAL_I2C_STATE_READY)))) /*!< Mask State define, keep only RX and TX bits            */
#define I2C_STATE_NONE            ((uint32_t)(HAL_I2C_MODE_NONE))                                                        /*!< Default Value                                          */
#define I2C_STATE_MASTER_BUSY_TX  ((uint32_t)(((uint32_t)HAL_I2C_STATE_BUSY_TX & I2C_STATE_MSK) | (uint32_t)HAL_I2C_MODE_MASTER))            /*!< Master Busy TX, combinaison of State LSB and Mode enum */
#define I2C_STATE_MASTER_BUSY_RX  ((uint32_t)(((uint32_t)HAL_I2C_STATE_BUSY_RX & I2C_STATE_MSK) | (uint32_t)HAL_I2C_MODE_MASTER))            /*!< Master Busy RX, combinaison of State LSB and Mode enum */
#define I2C_STATE_SLAVE_BUSY_TX   ((uint32_t)(((uint32_t)HAL_I2C_STATE_BUSY_TX & I2C_STATE_MSK) | (uint32_t)HAL_I2C_MODE_SLAVE))             /*!< Slave Busy TX, combinaison of State LSB and Mode enum  */
#define I2C_STATE_SLAVE_BUSY_RX   ((uint32_t)(((uint32_t)HAL_I2C_STATE_BUSY_RX & I2C_STATE_MSK) | (uint32_t)HAL_I2C_MODE_SLAVE))             /*!< Slave Busy RX, combinaison of State LSB and Mode enum  */

HAL_StatusTypeDef I2C_WaitOnFlagUntilTimeout(I2C_HandleTypeDef *hi2c, uint32_t Flag, FlagStatus Status, uint32_t Timeout, uint32_t Tickstart);
HAL_StatusTypeDef I2C_WaitOnMasterAddressFlagUntilTimeout(I2C_HandleTypeDef *hi2c, uint32_t Flag, uint32_t Timeout, uint32_t Tickstart);
HAL_StatusTypeDef I2C_WaitOnTXEFlagUntilTimeout(I2C_HandleTypeDef *hi2c, uint32_t Timeout, uint32_t Tickstart);
HAL_StatusTypeDef I2C_WaitOnBTFFlagUntilTimeout(I2C_HandleTypeDef *hi2c, uint32_t Timeout, uint32_t Tickstart);

bool ssd1306_i2cError(void)
{
	return HAL_OK != I2C_error;
}

static void ssd1306_i2cStart_STM(void)
{
	uint32_t Tickstart = HAL_GetTick();
	const uint16_t Timeout = 100U;

    /* Process Locked */
    __HAL_LOCK(&hi2c1);

	I2C_error = 0;
    SET_BIT(hi2c1.Instance->CR1, I2C_CR1_START);
    /* Wait until SB flag is set */
    if (I2C_WaitOnFlagUntilTimeout(&hi2c1, I2C_FLAG_SB, RESET, Timeout, Tickstart) != HAL_OK) {
    	I2C_error = HAL_ERROR;
    }
    hi2c1.Instance->DR = I2C_7BIT_ADD_WRITE(s_sa);
    /* Wait until ADDR flag is set */
    if (I2C_WaitOnMasterAddressFlagUntilTimeout(&hi2c1, I2C_FLAG_ADDR, Timeout, Tickstart) != HAL_OK) {
    	I2C_error = HAL_ERROR;
    }
    /* Clear ADDR flag */
    __HAL_I2C_CLEAR_ADDRFLAG(&hi2c1);

	I2C_error = HAL_OK;
}

static void ssd1306_i2cStop_STM(void)
{
	uint32_t Tickstart = HAL_GetTick();
	const uint16_t Timeout = 100U;

    /* Wait until TXE flag is set */
    if (I2C_WaitOnTXEFlagUntilTimeout(&hi2c1, Timeout, Tickstart) != HAL_OK)
    {
      if (hi2c1.ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Generate Stop */
        SET_BIT(hi2c1.Instance->CR1, I2C_CR1_STOP);
      }
      I2C_error = HAL_ERROR;
    }

    /* Generate Stop */
    SET_BIT(hi2c1.Instance->CR1, I2C_CR1_STOP);

    hi2c1.State = HAL_I2C_STATE_READY;
    hi2c1.Mode = HAL_I2C_MODE_NONE;

    /* Process Unlocked */
    __HAL_UNLOCK(&hi2c1);
}

void ssd1306_i2cConfigure_STM(uint8_t arg)
{
}

static void ssd1306_i2cSendByte_STM(uint8_t data)
{
	uint32_t Tickstart = HAL_GetTick();
	const uint16_t Timeout = 100U;

    hi2c1.State       = HAL_I2C_STATE_BUSY_TX;
    hi2c1.Mode        = HAL_I2C_MODE_MASTER;
    hi2c1.ErrorCode   = HAL_I2C_ERROR_NONE;

    /* Wait until TXE flag is set */
    if (I2C_WaitOnTXEFlagUntilTimeout(&hi2c1, Timeout, Tickstart) != HAL_OK)
    {
      if (hi2c1.ErrorCode == HAL_I2C_ERROR_AF)
      {
        /* Generate Stop */
        SET_BIT(hi2c1.Instance->CR1, I2C_CR1_STOP);
      }
      I2C_error = HAL_ERROR;
    }

    /* Write data to DR */
    hi2c1.Instance->DR = data;
}

static void ssd1306_i2cSendBytes_STM(const uint8_t *buffer, uint16_t size)
{
	uint32_t Tickstart = HAL_GetTick();
	const uint16_t Timeout = 100U;

	const uint8_t *pBuffPtr = buffer;
	I2C_error = HAL_OK;
    while (size > 0U)
    {
      /* Wait until TXE flag is set */
      if (I2C_WaitOnTXEFlagUntilTimeout(&hi2c1, Timeout, Tickstart) != HAL_OK)
      {
        if (hi2c1.ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Generate Stop */
          SET_BIT(hi2c1.Instance->CR1, I2C_CR1_STOP);
        }
    	I2C_error = HAL_ERROR;
      }

      /* Write data to DR */
      hi2c1.Instance->DR = *pBuffPtr;

      /* Increment Buffer pointer */
      ++pBuffPtr;

      /* Update counter */
      --size;

      if ((__HAL_I2C_GET_FLAG(&hi2c1, I2C_FLAG_BTF) == SET) && (size != 0U)) {
        /* Write data to DR */
        hi2c1.Instance->DR = *pBuffPtr;

        /* Increment Buffer pointer */
        ++pBuffPtr;

        /* Update counter */
        --size;
      }
      /* Wait until BTF flag is set */
      if (I2C_WaitOnBTFFlagUntilTimeout(&hi2c1, Timeout, Tickstart) != HAL_OK) {
        if (hi2c1.ErrorCode == HAL_I2C_ERROR_AF)
        {
          /* Generate Stop */
          SET_BIT(hi2c1.Instance->CR1, I2C_CR1_STOP);
        }
        I2C_error =  HAL_ERROR;
      }
    }

}


static void ssd1306_i2cClose_STM()
{

}

void ssd1306_i2cInit_STM(uint8_t sa)
{
    if (sa) s_sa = sa<<1;
    ssd1306_intf.spi = 0;
    ssd1306_intf.start = ssd1306_i2cStart_STM;
    ssd1306_intf.stop = ssd1306_i2cStop_STM;
    ssd1306_intf.send = ssd1306_i2cSendByte_STM;
    ssd1306_intf.send_buffer = ssd1306_i2cSendBytes_STM;
    ssd1306_intf.close = ssd1306_i2cClose_STM;
}

bool is_ssd1306_present(void)
{
	ssd1306_i2cInit();
	ssd1306_sendCommand(SSD1306_DISPLAYOFF);
	return !ssd1306_i2cError();
}
#endif


