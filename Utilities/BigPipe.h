
#ifndef BigPipe_H
#define BigPipe_H

#define BLOCK_OPS	1

#include <stdio.h>
#include <string.h>

#ifndef _Min
  template <typename T> const T& _Min (const T& a, const T& b) {
    return !(b<a)?a:b;     // or: return !comp(b,a)?a:b; for version (2)
  }
#endif

//## class BigPipe
template <uint16_t cFIFOSize, typename DT> class BigPipe {
    ////    Operations    ////

public :

    //## operation Flush()
    void Flush();

    //## operation GetBufferSize()
    uint16_t GetBufferSize();

    //## operation GetFilled() const
    uint16_t GetFilled() const;

    //## operation GetFree() const
    uint16_t GetFree() const;

    //## operation Read(DT)
    bool Read(DT & DataOut);

#if BLOCK_OPS
    //## operation Read(DT,u8)
    virtual uint16_t Read(DT* pData, const uint16_t Elements);
#endif
    //## operation Write(DT)
    bool Write(const DT& DataIn);

#if BLOCK_OPS
    //## operation Write(DT,u8)
    uint16_t Write(const DT * pData, const uint16_t Elements);
#endif
    ////    Attributes    ////

private :

    uint16_t m_ReadIndex;		//## attribute m_ReadIndex

    uint16_t m_WriteIndex;		//## attribute m_WriteIndex

    DT m_aByteBuffer[cFIFOSize];		//## attribute m_aByteBuffer
};

//## class BigPipe
template <uint16_t cFIFOSize, typename DT> void BigPipe<cFIFOSize, DT>::Flush() {
    //#[ operation Flush()
    m_ReadIndex=0U;
    m_WriteIndex=0U;
    //#]
}

template <uint16_t cFIFOSize, typename DT> uint16_t BigPipe<cFIFOSize, DT>::GetBufferSize() {
    //#[ operation GetBufferSize()
    return cFIFOSize;
    //#]
}

template <uint16_t cFIFOSize, typename DT> uint16_t BigPipe<cFIFOSize, DT>::GetFilled() const {
    //#[ operation GetFilled() const
    int16_t Delta= m_WriteIndex-m_ReadIndex;
    if (0 > Delta) {
    	Delta+=cFIFOSize;
    }
    return static_cast<uint16_t>(Delta);
    //#]
}

template <uint16_t cFIFOSize, typename DT> uint16_t BigPipe<cFIFOSize, DT>::GetFree() const {
    //#[ operation GetFree() const
    int16_t Delta= m_ReadIndex - m_WriteIndex -1 ;	// keep one element free to avoid wrap arround
    if (0 >= Delta) {
    	Delta+=cFIFOSize;
    }
    return static_cast<uint16_t>(Delta);
    //#]
}

template <uint16_t cFIFOSize, typename DT> bool BigPipe<cFIFOSize, DT>::Read(DT & DataOut) {
    //#[ operation Read(DT)
    bool Res=false;
    if (0U < GetFilled()) {
    	DataOut=m_aByteBuffer[m_ReadIndex];
        m_ReadIndex++;
        if (cFIFOSize <= m_ReadIndex) {
    		m_ReadIndex=0U;
    	}
    	Res=true;
    }
    return Res;

    //#]
}

#if BLOCK_OPS
template <uint16_t cFIFOSize, typename DT> uint16_t BigPipe<cFIFOSize, DT>::Read(DT* pData, const uint16_t Elements) {
    //#[ operation Read(DT,u8)
	uint16_t RetElements=0U;
	uint16_t ReadSize = _Min(GetFilled(),Elements);
	uint16_t part=cFIFOSize-m_ReadIndex;  	// number of elements  from read-ptr until end of buffer
    if (ReadSize <= part) {		// enough to complete Read in one pice?
    	memcpy(pData,&m_aByteBuffer[m_ReadIndex],ReadSize*sizeof(DT));
    	m_ReadIndex+=ReadSize;
    	RetElements=ReadSize;
    }
    else {
    	// read all data from read-ptr until end of buffer
    	memcpy(pData,&m_aByteBuffer[m_ReadIndex],part*sizeof(DT));
    	RetElements=part;
    	pData+=part;
    	// read-ptr points now to begin of buffer

    	part=ReadSize-part;	// number of remainig elements
    	if (m_WriteIndex < part)  {  // number of require elements still in buffer (begin of buffer(0): write-ptr)
    		part=m_WriteIndex;		// number of elements still in buffer is smaller
    	}
    	memcpy(pData,&m_aByteBuffer,part*sizeof(DT));	// copy elements
    	m_ReadIndex=part;		// move read-ptr behind last read element
    	RetElements+=part;
    }
    return RetElements;
    //#]
}
#endif

template <uint16_t cFIFOSize, typename DT> bool BigPipe<cFIFOSize, DT>::Write(const DT& DataIn) {
    //#[ operation Write(DT)
    bool Res=false;
    if (0U < GetFree()) {
    	m_aByteBuffer[m_WriteIndex]=DataIn;
        m_WriteIndex++;
        if (cFIFOSize <= m_WriteIndex) {
    		m_WriteIndex=0U;
    	}
    	Res=true;
    }
    return Res;

    //#]
}

#if BLOCK_OPS
template <uint16_t cFIFOSize, typename DT> uint16_t BigPipe<cFIFOSize, DT>::Write(const DT * pData, const uint16_t Elements) {
    uint16_t part=cFIFOSize-m_WriteIndex;  // number of elements  from write-ptr until end of buffer
    uint16_t Storable= _Min(GetFree(),Elements);

    if (Storable < part) {	// enough to complete Read in one piece?
    								// 1byte + to avoid overlap of ReadPtr and WrtPtr
    	memcpy(&m_aByteBuffer[m_WriteIndex],pData,Storable*sizeof(DT));
    	m_WriteIndex+=Storable;
    }
    else {
    	// read all data from read-ptr until end of buffer
    	memcpy(&m_aByteBuffer[m_WriteIndex],pData,part*sizeof(DT));
    	pData+=part;
    	// write-ptr points now to begin of buffer	--> m_WriteIndex=0

    	part=Storable-part;	// number of remaining elements
    	if (0U < part) {   // still something left to store and at least one element free?
    		memcpy(&m_aByteBuffer,pData,part*sizeof(DT));	// copy elements
    	}
    	m_WriteIndex=part;		// move write-ptr behind last written element
    }
    return Storable;
}
#endif

#undef Min

#endif
