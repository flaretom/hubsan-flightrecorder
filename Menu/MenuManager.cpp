/*
 * MenuManager.cpp
 *
 *  Created on: 16.01.2020
 *      Author: tom
 */

#include <Menu/MenuManager.h>
#include <Menu/MenuEntry.h>
#include <Menu/IDisplayBase.h>
#include <Menu/IDataStorage.h>
#include <stdio.h>
#include <string.h>

MenuManager::~MenuManager() {
	// TODO Auto-generated destructor stub
}

const bool cAutoReturn = true;

void MenuManager::Message(const char * const text, uint32_t  )
{
    m_pDisplay->printFixed( 0, 0, text, IDisplayBase::eMessage);
}

const uint8_t cStartRow = 2U;

void MenuManager::Show(bool clean)
{
    m_pDisplay->clear(clean);
    m_Column = 0;
    m_Row = cStartRow;
    const MenuEntry_t * const Menu = m_MenuStack[m_MenuLevel];
    m_pDisplay->printFixed( 0, 0, FilterSpecialField(Menu->title), IDisplayBase::eTitle);
    for ( int nEntry = cAutoReturn? -1: 0; MenuEntry_t::MaxEntries > nEntry; ++nEntry ){
        if (-1 == nEntry)  {
        	if (0 < m_MenuLevel) {
				if (nEntry == (int)m_Selected) {
					if (!m_pDisplay->drawBitmap(112,0, 0)) {
						m_pDisplay->printFixed( 3, 0, "Return", IDisplayBase::eSelected);
					}
				}
				else {
					if (!m_pDisplay->drawBitmap(112,0, 0, true)) {
						m_pDisplay->printFixed( 3, 0, "Return", IDisplayBase::eNormal);
					}
				}
        	}
            else {
                if (-1 == m_Selected) {
                    m_Selected = 0;
                }
            }
        }
        else {
            const MenuEntry_t * ME = Menu->submenu[nEntry];
            if (nullptr != ME) {
                const char *text = ME->title;
                text += ShowSpecialField(text, ME);
                if (nullptr != ME->command) {
                    const char * altText = ME->command(ME, eUpdateDisplay);
                    if (nullptr != altText) {
                        text = altText;
                    }
                }
                if (nullptr != text) {
                    if (nEntry == (int)m_Selected) {
                        m_pDisplay->printFixed( m_Column, m_Row, text, IDisplayBase::eSelected);
                    }
                    else {
                        m_pDisplay->printFixed( m_Column, m_Row, text, IDisplayBase::eNormal);
                    }
                }
                else {
                    m_pDisplay->printFixed( m_Column, m_Row, "-" , IDisplayBase::eNormal);
                }
            }
            else {
                m_pDisplay->printFixed( m_Column, m_Row, "" , IDisplayBase::eNormal);
            }
            ++m_Row;
        }
	}
    m_pDisplay->update();
}

uint32_t MenuManager::ShowSpecialField(const char * text, const MenuEntry_t * const MenuPt)
{
    uint32_t textInc = 0;
    bool bItemFound = false;
    do {
        bItemFound = false;
        if ('#' == text[textInc]) {   // checkbox
            if (nullptr != m_Storage) {
                int32_t val = m_Storage->getData(MenuPt);
                if (0==val) {
                    m_pDisplay->printFixed( m_Column + 1, m_Row, "O", IDisplayBase::eNormal);
                }
                else {
                    m_pDisplay->printFixed( m_Column + 1 , m_Row, "+", IDisplayBase::eNormal);
                }
            }
            bItemFound = true;
            ++textInc;
        }
        else if ('+' == text[textInc]) {   // integer
            if (nullptr != m_Storage) {
                int32_t val = m_Storage->getData(MenuPt);
                char szBuffer[10];
                sprintf(szBuffer, "%i",(int)val);
                m_pDisplay->printFixed( m_Column + 1 , m_Row, szBuffer, IDisplayBase::eNormal);
            }
            bItemFound = true;
            ++textInc;
        }
        else if ('|' == text[textInc]) {   // change column
            m_Column += 2;
            m_Row = cStartRow;
            bItemFound = true;
            ++textInc;
        }
        else if ('_' == text[textInc]) {   // change column
            ++m_Row;
            bItemFound = true;
            ++textInc;
        }
        else if ('-' == text[textInc]) {   // no selectable
            bItemFound = true;
            ++textInc;
        }
        else if ('\\' == text[textInc]) {   // ignore rest of line
            bItemFound = false;
            ++textInc;
        }
        else if ('@' == text[textInc]) {   // position
            int col = text[textInc+1]-'0';
            int row = text[textInc+2]-'0';
            m_Column = col;
            m_Row = row;
            bItemFound = true;
            textInc+=3;
        }
    } while(bItemFound);
    return textInc;
}

const char * MenuManager::FilterSpecialField(const char * text)
{
	const char *p = text;
	while (nullptr != strchr("\\-_|+#", *p)) {
		++p;
	}
	return p;
}

bool MenuManager::UpdateSpecialField(const char * text, const MenuEntry_t * const MenuPt)
{
    bool handled = false;
    if ('#' == text[0]) {   // checkbox
        if (nullptr != m_Storage) {
            int32_t val = m_Storage->getData(MenuPt);
            m_Storage->setData(MenuPt, !val);
        }
        handled = true;
    }
    if ('+' == text[0]) {   // checkbox
        if (nullptr != m_Storage) {
            int32_t val = m_Storage->getData(MenuPt);
            ++val;
            m_Storage->setData(MenuPt, val);
        }
        handled = true;
    }
    if ('-' == text[0]) {   // checkbox
        handled = true;
    }
    if ('@' == text[0]) {   // checkbox
        handled = true;
    }
    return handled;
}

void MenuManager::Select()
{
    const MenuEntry_t * const Menu = m_MenuStack[m_MenuLevel];
    const MenuEntry_t * Submenu = nullptr;
    if (0 <= m_Selected) {
    	Submenu = Menu->submenu[m_Selected];
    }

    if (nullptr != Submenu) {
        const MenuEntry_t * const Submenu = Menu->submenu[m_Selected];
        if (nullptr != Submenu->command) {
            Submenu->command(Submenu, eSelectKey);
        }
        if (!UpdateSpecialField(Submenu->title, Submenu)){
            if (nullptr != Submenu->submenu[0]) {       // anything to go to?
                ++m_MenuLevel;
                m_MenuStack[m_MenuLevel] = Submenu;
                m_Selected = cAutoReturn? -1: 0;
            }
        }
    }
    else {
        Return();
    }
    Show();
}

void MenuManager::Up()
{
    const MenuEntry_t * const Menu = m_MenuStack[m_MenuLevel];
    const MenuEntry_t * Submenu = nullptr;
    if (0 <= m_Selected) {
    	Submenu = Menu->submenu[m_Selected];
    }
    if ( (nullptr != Submenu) && (nullptr != Submenu->command)) {
        Submenu->command(Submenu, eUpKey);
    }
    if (m_Selected) {
        --m_Selected;
    }
    Show();
}

void MenuManager::Down()
{
    const MenuEntry_t * const Menu = m_MenuStack[m_MenuLevel];
    const MenuEntry_t * Submenu = nullptr;
    if (0 <= m_Selected) {
    	Submenu = Menu->submenu[m_Selected];
    }
	if ( (nullptr != Submenu) && (nullptr != Submenu->command)) {
		Submenu->command(Submenu, eDownKey);
	}
    do {
        ++m_Selected;
        if (nullptr == Menu->submenu[m_Selected] || MenuEntry_t::MaxEntries == m_Selected) {     // end of submenu entries reached
            m_Selected = cAutoReturn? -1: 0;
            break;
        }
    } while ('-' == Menu->submenu[m_Selected]->title[0]);
    Show();
}

void MenuManager::Return()
{
    const MenuEntry_t * const Menu = m_MenuStack[m_MenuLevel];
    if ( (nullptr != Menu) && (nullptr != Menu->command)) {
        Menu->command(Menu, eReturnKey);
    }
    const MenuEntry_t * Submenu = nullptr;
    if (0 <= m_Selected) {
    	Submenu = Menu->submenu[m_Selected];
    }
//	if ( (nullptr != Submenu) && (nullptr != Submenu->command)) {
//		Submenu->command(Submenu, eReturnKey);
//	}
//	else {
		if (m_MenuLevel) {
			--m_MenuLevel;
		}
		Show();
//	}
}
