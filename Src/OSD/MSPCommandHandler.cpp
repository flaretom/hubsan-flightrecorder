/*
 * MSPCommandHandler.cpp
 *
 *  Created on: 26.10.2019
 *      Author: tom
 */

#include <OSD/MSPCommandHandler.h>
#include "stm32f1xx_hal.h"
#include "usart.h"
#include <string.h>
#include <algorithm>

// Chip Unique ID on F103
#define U_ID_0 (*(uint32_t*)0x1FFFF7E8)
#define U_ID_1 (*(uint32_t*)0x1FFFF7EC)
#define U_ID_2 (*(uint32_t*)0x1FFFF7F0)

#define FC_VERSION_MAJOR			0
#define FC_VERSION_MINOR			1
#define FC_VERSION_PATCH_LEVEL		1

#ifndef TARGET_DEFAULT_MIXER
#define TARGET_DEFAULT_MIXER    MIXER_QUADX
#endif

extern "C" {
	uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);
	uint8_t CDC_Transmit_Busy();
}

const char *MSPCommandHandler::pcHead="$M<";

uint64_t MSPCommandHandler::m_activeBoxIds = (1 << BOXARM) | (1 << BOXANGLE);

typedef void serializeBoxFn(MSPCommandHandler *pHdl, const MSPCommandHandler::box_t *box);
static void serializeBoxReply(MSPCommandHandler *pHdl, int page, serializeBoxFn *serializeBox);
static void serializeBoxPermanentIdFn(MSPCommandHandler *pHdl, const MSPCommandHandler::box_t *box);
static void serializeBoxNameFn(MSPCommandHandler *pHdl, const MSPCommandHandler::box_t *box);

void MSPCommandHandler::Reset()
{
	// prepare receiving of MSP messages for OSD
	if (nullptr != m_pUart) {
		HAL_UART_Receive_IT(m_pUart, m_MSP_HeadBuffer, cMSP_HeaderLength);
	}
	m_MSP_Length = 0U;
	++m_MSP_command_errors;
	m_MSP_State = eMSP_Header;
}

void MSPCommandHandler::Init(const FlightData_t *pFD, MSPCommandHandler * pPassThroughPeer)
{
	m_pFD = pFD;
	m_pPassThroughPeer = pPassThroughPeer;
	Reset();
}

bool MSPCommandHandler::SendSerial(uint8_t &byte)
{
	bool a = m_ResponsePipe.Read(byte);
	if (!a) {
		if (nullptr == m_pUart) {
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
		}
		else {
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_RESET);
		}
	}
	return a;
}

void MSPCommandHandler::ReceiveSerial(const uint8_t byte)
{
	if (eMSP_Header == m_MSP_State) {
		if ('$' == byte ) {
			m_MSP_RxBufferIdx = 0U;
			m_MSP_HeadBuffer[m_MSP_RxBufferIdx] = byte;
			++m_MSP_RxBufferIdx;
			m_MSP_State = eMSP_Header;
		}
		else {
			if (cMSP_HeaderLength > m_MSP_RxBufferIdx) {
				m_MSP_HeadBuffer[m_MSP_RxBufferIdx] = byte;
				++m_MSP_RxBufferIdx;
			}
			if (cMSP_HeaderLength == m_MSP_RxBufferIdx) {
				if (0==memcmp(m_MSP_HeadBuffer,pcHead,3)) {
					m_MSP_DataBuffer[0] = m_MSP_HeadBuffer[3];
					// receive message payload, keep 1 byte free in m_MSP_DataBuffer for length
					//HAL_UART_Receive_IT(&huart3, m_MSP_DataBuffer +1 , m_MSP_HeadBuffer[3] + 2U); // 2 -> command + 'crc'
					m_MSP_RxBufferIdx = 1U;
					m_MSP_State = eMSP_Data;
					++m_MSP_command_hcount;
				}
				else {
					m_MSP_RxBufferIdx = 0U;
				}
			}
		}
	}
	else {
		m_MSP_DataBuffer[m_MSP_RxBufferIdx] = byte;
		if (m_MSP_HeadBuffer[3] + 1U >= m_MSP_RxBufferIdx) {
			++m_MSP_RxBufferIdx;
		}
		else {
			const uint8_t MsgSize = m_MSP_DataBuffer[0];
			uint8_t Xor = 0U;
			for (auto i = 0U; MsgSize+3U > i; ++i) {		// +3 --> Length, command, crc
				Xor ^= m_MSP_DataBuffer[i];
			}
			if(0U == Xor) {
				if ((2U == m_SerialPassThroughStage) && (nullptr != m_pPassThroughPeer)) {
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
					m_pPassThroughPeer->transferBuffer(m_MSP_HeadBuffer, cMSP_HeaderLength);
					m_pPassThroughPeer->transferBuffer(m_MSP_DataBuffer + 1U, MsgSize + 2U);
					m_pPassThroughPeer->USBSend();
				}
				else {
					// handle message
					if (PrepResponse(m_MSP_DataBuffer[1])) {
						HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);
						__HAL_UART_ENABLE_IT(m_pUart, UART_IT_TXE);	// start transmitting response
					}
				}
			}
			Reset();
		}
	}
}

int constexpr length(const char* str)
{
    return *str ? 1 + length(str + 1) : 0;
}
const char * const CLIPrompt = "\r\nCLI\r\n#";
const uint8_t CLIPromptLen = length(CLIPrompt);
const char * const pcStartPassThrough = "serialpassthrough";
const uint8_t StartPassThroughLen = length(pcStartPassThrough);
const char * const CLIPort1 = "Port 1 opened, baud = 115200.\r\n";
const uint8_t CLIPortLen1 = length(CLIPort1);
const char * const CLIPort2 = "Forwarding, power cycle to exit.\r\n";
const uint8_t CLIPortLen2 = length(CLIPort2);
const char * const ENDCLI = "endcli";
const uint8_t ENDCLILen = length(ENDCLI);

void MSPCommandHandler::ReceiveUsb(uint8_t *pBuffer, uint32_t Len)
{
	if (0 == strncmp(reinterpret_cast<char*>(pBuffer), ENDCLI, ENDCLILen)) {
		m_pPassThroughPeer->stopPassthrough();
		m_SerialPassThroughStage = 0;
	}
	else if (*pBuffer=='#') {	// CLI requested
		transferBuffer(reinterpret_cast<const uint8_t*>(CLIPrompt), CLIPromptLen);
		USBSend();
		m_SerialPassThroughStage = 1;
		m_pPassThroughPeer->stopPassthrough();
	}
	else {
		if (0 == m_SerialPassThroughStage) {
			if (0 == strcmp(reinterpret_cast<char*>(pBuffer), pcHead)) {
				m_CommandPipe.Write(Len - 3U);
				for (uint8_t i = 0U; Len-3U > i; ++i) {
					m_CommandPipe.Write(pBuffer[3U +i]);
				}
			}
		}
		else if (1 == m_SerialPassThroughStage){
			if (0 == strncmp(reinterpret_cast<char*>(pBuffer), pcStartPassThrough, StartPassThroughLen)) {
				m_SerialPassThroughStage = 2U;
				m_SerialResponseStage = 1;
			}
			else {
				m_SerialPassThroughStage = 0;
			}
		}
		else if (2 == m_SerialPassThroughStage){	// can only happen on USB side (nullptr == m_pUart)
			if (nullptr != m_pPassThroughPeer) {
				m_pPassThroughPeer->transferBuffer(pBuffer, Len);
			}
		}
	}
}

void MSPCommandHandler::stopPassthrough()
{
	m_SerialPassThroughLength = 0U;
	m_SerialPassThroughStage = 0;
}

void MSPCommandHandler::transferBuffer(const uint8_t *pBuffer, const uint32_t Len)
{
	uint8_t l = Len;
	if (0 <= l && nullptr != pBuffer) {
		while (l) {
			m_ResponsePipe.Write(*pBuffer);
			++pBuffer;
			--l;
		}
		//m_ResponsePipe.Write(pBuffer, Len);
		if (nullptr != m_pUart) {
			__HAL_UART_ENABLE_IT(m_pUart, UART_IT_TXE);	// start transmitting response
		}
		//memcpy(m_MSP_RespBuffer, pBuffer, std::min(Len,static_cast<uint32_t>(sizeof(m_MSP_RespBuffer))));
	}
	m_SerialPassThroughLength = static_cast<uint8_t>(Len);
	m_SerialPassThroughStage = 2;
}

void MSPCommandHandler::USBSend()
{
	if (!CDC_Transmit_Busy()) {
		uint16_t len = m_ResponsePipe.GetFilled();
		if (len) {
			uint8_t *pBuffer = m_MSP_RespBuffer;
			const uint8_t Len = std::min(len, static_cast<uint16_t>(64U));
			for (uint8_t i = 0U; Len > i; ++i) {
				uint8_t byte;
				m_ResponsePipe.Read(byte);
				*pBuffer = byte;
				++pBuffer;
			}
			CDC_Transmit_FS(m_MSP_RespBuffer, len);
		}
	}
}


void MSPCommandHandler::Cyclic()
{
	if (nullptr == m_pUart) {		// USB only
		if (!CDC_Transmit_Busy()) {
			switch(m_SerialResponseStage) {
			case 1:
				transferBuffer(reinterpret_cast<const uint8_t*>(CLIPort1), CLIPortLen1);
				USBSend();
				++m_SerialResponseStage;
				break;
			case 2:
				transferBuffer(reinterpret_cast<const uint8_t*>(CLIPort2), CLIPortLen2);
				USBSend();
				++m_SerialResponseStage;
				break;
			case 3:
				m_pPassThroughPeer->transferBuffer(nullptr, 0U);
				m_SerialResponseStage = 0;
				break;
			}
		}
	}
}

#if 0

//		if (2 == m_SerialPassThroughStage){
//			if (0U < m_SerialPassThroughLength) {
//				//HAL_UART_Transmit_IT(m_pUart, m_MSP_RespBuffer, m_SerialPassThroughLength);
//				m_SerialPassThroughLength = 0U;
//			}
//			if (m_MSP_Length) {
//				const uint8_t MsgSize = m_MSP_HeadBuffer[3] + 2U;
//				memcpy(m_MSP_RespBuffer, m_MSP_HeadBuffer, cMSP_HeaderLength);
//				memcpy(m_MSP_RespBuffer + cMSP_HeaderLength, m_MSP_DataBuffer, MsgSize);
//				CDC_Transmit_FS(m_MSP_RespBuffer, MsgSize + cMSP_HeaderLength);
//				if (nullptr != m_pUart) {
//					HAL_UART_Receive_IT(m_pUart, m_MSP_HeadBuffer, cMSP_HeaderLength);
//				}
//				m_MSP_State = eMSP_Header;
//				m_MSP_Length = 0U;
//			}
//		}
//	}
//	else {	// USB port
//	}
	if (2 == m_SerialPassThroughStage){
		uint16_t len = m_ResponsePipe.GetFilled();
		if (nullptr != m_pUart) {		// serial port
			__HAL_UART_ENABLE_IT(m_pUart, UART_IT_TXE);	// start transmitting response
		}
		else {
			if (0U < len) {
//				m_ResponsePipe.Read(m_MSP_RespBuffer, std::min(len, static_cast<uint16_t>(64U)));
				uint8_t *pBuffer = m_MSP_RespBuffer;
				uint8_t Len = std::min(len, static_cast<uint16_t>(64U));
				while (Len) {
					m_ResponsePipe.Read(*pBuffer);
					++pBuffer;
					--Len;
				}
				CDC_Transmit_FS(m_MSP_RespBuffer, len);
			}
			else {
				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
			}
		}
	}
}
#endif

void MSPCommandHandler::setOSDLayout(uint8_t OSD_Switch)
{
	m_OSD_Switch = OSD_Switch;
}

int MSPCommandHandler::PrepResponse(uint8_t MSP_command)
{
	m_WrtPtr = 0;
	if ( (0U != MSP_command) && (nullptr != m_pFD)) {
		++m_MSP_command_handled;
		switch (MSP_command) {
		case MSP_STATUS:
			startMsg(MSP_command);
			put16(m_MSP_command_handled);	// cycletime us
			put16(m_MSP_command_errors);	// I2C error count
			put16(3U);						// BARO<<1|MAG<<2|GPS<<3|SONAR<<4
			put32(0U);						// a bit variable to indicate which BOX are active, the bit position depends on the BOX which are configured
			put8(3U);						// global_conf.currentSet, to indicate the current configuration setting
			finish();
			break;
//		case MSP_STATUS_EX:
//		case MSP_STATUS:
//			startMsg(MSP_command);
//	        put16(10);		// getTaskDeltaTime(TASK_SERIAL)
//	        put16(0);		// i2cGetErrorCounter
//	        put16(0); // sensors
//	        put32(0); // flight modes
//	        put8( 0); // profile
//	        put16(50);	// constrain(averageSystemLoadPercent, 0, 100)
//	        if (MSP_STATUS_EX == MSP_command) {
//	        	put8(1); // max profiles
//	        	put8(0); // control rate profile
//	        } else {
//	            put16(0); // gyro cycle time
//	        }
//			finish();
//	        break;
		case MSP_RAW_IMU:
			startMsg(MSP_command);
			put16(0U);	// accx
			put16(0U);	// accy
			put16(0U);	// accz
			put16(0U);	// gyrx
			put16(0U);	// gyry
			put16(0U);	// gyrz
			put16(0U);	// magx
			put16(0U);	// magy
			put16(0U);	// magz
			finish();
			break;
		case MSP_RC: {
			startMsg(MSP_command);
			for (int i = 0; 8>i; ++i) {
				if (7 == i) {
					put16(1300 + m_OSD_Switch * 200);	// rcData for channel 7 --> osd switch
				}
				else {
					put16(0);		// rcData
				}
			}
			finish();
			}
			break;
		case MSP_RAW_GPS:
			startMsg(MSP_command);
			put8(m_pFD->Sat > 0 ? 1:0);	// GPS fix
			put8(m_pFD->Sat);				// GPS Sat
			put32(m_pFD->latit);			// GPS longitude
			put32(m_pFD->longit);			// GPS latitude
			put16(m_pFD->elevat);		// GPS distance to home
			put16(1000);					// speed
			put16(45);					// ground course
			finish();
			break;
		case MSP_COMP_GPS:
			startMsg(MSP_command);
			put16(m_pFD->distance);	// GPS distance to home
			put16(0U);	// GPS direction to home
			put8(0U);	// GPS update
			finish();
			break;
		case MSP_ATTITUDE:
			startMsg(MSP_command);
			put16(m_pFD->R);	// roll(16)
			put16(m_pFD->P);	// pitch(16)
			put16(m_pFD->H/10);	// heading
			finish();
			break;
		case MSP_ANALOG:
			startMsg(MSP_command);
			put8(m_pFD->VBat);	// VBat
			put16(0U);			// intPowerMeterSum
			put16(0U);			// rssi
			put16(0U);			// amperage
			finish();
			break;
		case MSP_ALTITUDE:
			startMsg(MSP_command);
			put32(m_pFD->elevat);	// elevation
			put16(0U);			// vario
			finish();
			break;
		case MSP_IDENT:
			startMsg(MSP_command);
			put8(2U);			// MultiWii Firmware version
			finish();
			break;
		case MSP_FC_VARIANT:
			startMsg(MSP_command);
			putN((const uint8_t*)flightControllerIdentifier, 4U);			// MultiWii Firmware version
			finish();
			break;
		case MSP_FC_VERSION:
			startMsg(MSP_command);
	        put8(FC_VERSION_MAJOR);
	        put8(FC_VERSION_MINOR);
	        put8(FC_VERSION_PATCH_LEVEL);
			finish();
			break;
	    case MSP_BUILD_INFO:
			startMsg(MSP_command);
	        putN((const uint8_t*)buildDate, BUILD_DATE_LENGTH);
	        putN((const uint8_t*)buildTime, BUILD_TIME_LENGTH);
	        putN((const uint8_t*)shortGitRevision, GIT_SHORT_REVISION_LENGTH);
			finish();
	        break;
		case MSP_BOARD_INFO:
			startMsg(MSP_command);
			putN((const uint8_t*)BoardIdentifier, 4U);			// MultiWii Firmware version
			put16(0x0001);						// hardwareRevision
			put8(2U);  							// 2 == FC with OSD
			put8(0U);								// commCapabilities
			put8(strlen(TargetName));
			putStr(TargetName);
			put8(strlen(BoardName));
			putStr(BoardName);
			put8(strlen(Manufacturer));
			putStr(Manufacturer);
			finish();
			break;
	    case MSP_MIXER_CONFIG:
			startMsg(MSP_command);
	        put8(TARGET_DEFAULT_MIXER);	// mixerConfig()->mixerMode
	        put8(0U); // mixerConfig()->yaw_motors_reversed = false
			finish();
	        break;
	    case MSP_FEATURE_CONFIG:
			startMsg(MSP_command);
	        put32(DEFAULT_FEATURES | DEFAULT_RX_FEATURE | FEATURE_DYNAMIC_FILTER | FEATURE_ANTI_GRAVITY | FEATURE_TELEMETRY );
			finish();
	        break;
		case MSP_UID:
			startMsg(MSP_command);
	        put32(U_ID_0);
	        put32(U_ID_1);
	        put32(U_ID_2);
			finish();
	        break;
		case MSP_BOXIDS:
			{
				startMsg(MSP_command);
				const int page = 0; // sbufBytesRemaining(src) ? sbufReadU8(src) : 0;
				serializeBoxReply(this, page, &serializeBoxPermanentIdFn);
				finish();
			}
			break;
	    case MSP_BOXNAMES:
	    	{
				startMsg(MSP_command);
				const int page = 0; //sbufBytesRemaining(src) ? sbufReadU8(src) : 0;
				serializeBoxReply(this, page, &serializeBoxNameFn);
				finish();
	    	}
	        break;
	    case MSP_MOTOR_3D_CONFIG:
			startMsg(MSP_command);
//	        sbufWriteU16(dst, flight3DConfig()->deadband3d_low);
//	        sbufWriteU16(dst, flight3DConfig()->deadband3d_high);
//	        sbufWriteU16(dst, flight3DConfig()->neutral3d);
			finish();
	        break;
		case 0xFFU:
		default:
			--m_MSP_command_handled;
			break;
		case 246:
			break;
		}
	}
	return m_WrtPtr;
}

void MSPCommandHandler::startMsg(uint8_t Msgtype)
{
	m_WrtPtr = 0U;
	m_pWrtBuffer[m_WrtPtr++] = '$';
	m_pWrtBuffer[m_WrtPtr++] = 'M';
	m_pWrtBuffer[m_WrtPtr++] = '>';
	m_pWrtBuffer[m_WrtPtr++] = 0U;
	m_pWrtBuffer[m_WrtPtr++] = Msgtype;
	m_XOR = Msgtype;
}

void MSPCommandHandler::put32(uint32_t val)
{
	put16(val & 0xFFFFU);
	put16(val >> 16);
}

void MSPCommandHandler::put16(uint16_t val)
{
	put8(val & 0xFFU);
	put8(val >> 8);
}

void MSPCommandHandler::put8(uint8_t val)
{
	m_XOR ^= val;
	m_pWrtBuffer[m_WrtPtr++] = val;
}

void MSPCommandHandler::putStr(const char * pStr)
{
	while (pStr && *pStr) {
		put8(*pStr);
		++pStr;
	}
}

void MSPCommandHandler::putN(const uint8_t * pStr, const uint8_t Len)
{
	uint8_t bytes = Len;
	while (pStr && (0U < bytes)) {
		put8(*pStr);
		++pStr;
		--bytes;
	}
}

void MSPCommandHandler::finish()
{
	uint8_t MsgLength = m_WrtPtr - 5U;
	m_pWrtBuffer[3]= MsgLength;
	m_XOR ^= MsgLength;
	m_pWrtBuffer[m_WrtPtr++] = m_XOR;

	m_ResponsePipe.Write(m_pWrtBuffer, m_WrtPtr);
}

#define CLEANFLIGHT_IDENTIFIER "CLFL"
#define TARGET_BOARD_IDENTIFIER "TEST"
const char * const MSPCommandHandler::flightControllerIdentifier = CLEANFLIGHT_IDENTIFIER; // 4 UPPER CASE alpha numeric characters that identify the flight controller.
const char * const MSPCommandHandler::BoardIdentifier = TARGET_BOARD_IDENTIFIER; // 4 UPPER CASE alpha numeric characters that identify the flight controller.
const char * const MSPCommandHandler::TargetName="F103";
const char * const MSPCommandHandler::BoardName = "HubsanFlightRec";
const char * const MSPCommandHandler::Manufacturer = "flaretom";


// permanent IDs must uniquely identify BOX meaning, DO NOT REUSE THEM!
const MSPCommandHandler::box_t MSPCommandHandler::boxes[] = {
    { BOXARM, "ARM", 0 },
    { BOXANGLE, "ANGLE", 1 },
    { BOXHORIZON, "HORIZON", 2 },
    { BOXBARO, "BARO", 3 },
    { BOXANTIGRAVITY, "ANTI GRAVITY", 4 },
    { BOXMAG, "MAG", 5 },
    { BOXHEADFREE, "HEADFREE", 6 },
    { BOXHEADADJ, "HEADADJ", 7 },
    { BOXCAMSTAB, "CAMSTAB", 8 },
//    { BOXCAMTRIG, "CAMTRIG", 9 },
    { BOXGPSHOME, "GPS HOME", 10 },
    { BOXGPSHOLD, "GPS HOLD", 11 },
    { BOXPASSTHRU, "PASSTHRU", 12 },
    { BOXBEEPERON, "BEEPER", 13 },
//    { BOXLEDMAX, "LEDMAX", 14 }, (removed)
    { BOXLEDLOW, "LEDLOW", 15 },
//    { BOXLLIGHTS, "LLIGHTS", 16 }, (removed)
    { BOXCALIB, "CALIB", 17 },
//    { BOXGOV, "GOVERNOR", 18 }, (removed)
    { BOXOSD, "OSD DISABLE SW", 19 },
    { BOXTELEMETRY, "TELEMETRY", 20 },
//    { BOXGTUNE, "GTUNE", 21 }, (removed)
//    { BOXRANGEFINDER, "RANGEFINDER", 22 }, (removed)
    { BOXSERVO1, "SERVO1", 23 },
    { BOXSERVO2, "SERVO2", 24 },
    { BOXSERVO3, "SERVO3", 25 },
    { BOXBLACKBOX, "BLACKBOX", 26 },
    { BOXFAILSAFE, "FAILSAFE", 27 },
    { BOXAIRMODE, "AIR MODE", 28 },
    { BOX3D, "DISABLE / SWITCH 3D", 29},
    { BOXFPVANGLEMIX, "FPV ANGLE MIX", 30},
    { BOXBLACKBOXERASE, "BLACKBOX ERASE (>30s)", 31 },
    { BOXCAMERA1, "CAMERA CONTROL 1", 32},
    { BOXCAMERA2, "CAMERA CONTROL 2", 33},
    { BOXCAMERA3, "CAMERA CONTROL 3", 34 },
    { BOXFLIPOVERAFTERCRASH, "FLIP OVER AFTER CRASH", 35 },
    { BOXPREARM, "PREARM", 36 },
    { BOXBEEPGPSCOUNT, "BEEP GPS SATELLITE COUNT", 37 },
//    { BOX3DONASWITCH, "3D ON A SWITCH", 38 }, (removed)
    { BOXVTXPITMODE, "VTX PIT MODE", 39 },
    { BOXUSER1, "USER1", 40 },
    { BOXUSER2, "USER2", 41 },
    { BOXUSER3, "USER3", 42 },
    { BOXUSER4, "USER4", 43 },
    { BOXPIDAUDIO, "PID AUDIO", 44 },
    { BOXPARALYZE, "PARALYZE", 45 },
    { BOXGPSRESCUE, "GPS RESCUE", 46 },
    { BOXACROTRAINER, "ACRO TRAINER", 47 },
	{ 0, nullptr, 0 },
};

#define ARRAYLEN(x) (sizeof(x) / sizeof((x)[0]))

static bool activeBoxIdGet(boxId_e boxId)
{
    if (boxId > sizeof(uint64_t) * 8)
        return false;
    return MSPCommandHandler::getActiveBoxIds() & (1LL << boxId);
}

const MSPCommandHandler::box_t *findBoxByBoxId(boxId_e boxId)
{
    for (unsigned i = 0; i < ARRAYLEN(MSPCommandHandler::boxes); i++) {
        const MSPCommandHandler::box_t *candidate = &MSPCommandHandler::boxes[i];
        if (candidate->boxId == boxId)
            return candidate;
    }
    return NULL;
}

// serialize 'page' of boxNames.
// Each page contains at most 32 boxes
void serializeBoxReply(MSPCommandHandler *pHdl, int page, serializeBoxFn *serializeBox)
{
    unsigned boxIdx = 0;
    unsigned pageStart = page * 32;
    unsigned pageEnd = pageStart + 32;
    for (int id = 0; 64 > id; ++id) {
        if (activeBoxIdGet(static_cast<boxId_e>(id))) {
            if (boxIdx >= pageStart && boxIdx < pageEnd) {
                (*serializeBox)(pHdl,findBoxByBoxId(static_cast<boxId_e>(id)));
            }
            boxIdx++;                 // count active boxes
        }
    }
}

void serializeBoxPermanentIdFn(MSPCommandHandler *pHdl, const MSPCommandHandler::box_t *box)
{
    pHdl->put8(box->permanentId);
}

void serializeBoxNameFn(MSPCommandHandler *pHdl, const MSPCommandHandler::box_t *box)
{
	pHdl->putStr(box->boxName);
	pHdl->put8(';');
}
