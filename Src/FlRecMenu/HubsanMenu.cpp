/*
 * HubsanMenu.cpp
 *
 *  Created on: 16.01.2020
 *      Author: tom
 */


#include <Menu/MenuEntry.h>
#include <Menu/MenuManager.h>

#include "HubsanMenu.h"
#include "rtc.h"

#include <stdio.h>

#include "Calendar.h"
extern Calendar Cal;

MenuManager *s_pManager;
TestDataStorage Td;

const char* MenuReturn(const tagMenuEntry_t * const , int key)
{
    if ((nullptr != s_pManager) && (1==key)) {
        s_pManager->Return();
    }
    return nullptr;
}
MenuEntry_t returnSub = { "Return", MenuReturn, {} };

const char* ShowTime(const tagMenuEntry_t * const , int )
{
    static char szBuffer[32];
	char DateStr[20];
	char Timestamp[20];
	Cal.getDateStr(DateStr, sizeof(DateStr), '-');
	Cal.getTimeStr(Timestamp, sizeof(Timestamp), '-');
	sprintf(szBuffer, "%s %s",DateStr, Timestamp );
    return szBuffer;
}

const char* updateSettings(const tagMenuEntry_t * const , int key)
{
    if (MenuManager::eReturnKey == key) {
    	Td.writeSettings();
    }
    if (MenuManager::eSelectKey == key) {
    	Td.readSettings();
    }
    return nullptr;
}

const char* loadTime(const tagMenuEntry_t * const , int key)
{
    if (MenuManager::eSelectKey == key) {
    	Td.loadTime();
    }
    return nullptr;
}

const char* loadDate(const tagMenuEntry_t * const , int key)
{
    if (MenuManager::eSelectKey == key) {
    	Td.loadDate();
    }
    return nullptr;
}

const char* SetTime(const tagMenuEntry_t * const , int key)
{
    if (MenuManager::eSelectKey == key) {
//        SetTime();
        if (nullptr != s_pManager) {
            s_pManager->Message("SetTime");
            Td.setDateTime();
        }
    }
    return nullptr;
}

const char* SetDate(const tagMenuEntry_t * const , int key)
{
    if (MenuManager::eSelectKey == key) {
//        SetDate();
        if (nullptr != s_pManager) {
            s_pManager->Message("SetDate");
            Td.setDateTime();
        }
    }
    return nullptr;
}

const char* RestoreSettingsSD(const tagMenuEntry_t * const , int key)
{
    if (MenuManager::eSelectKey == key) {
//        Restore();
        if (nullptr != s_pManager) {
            s_pManager->Message("Restore");
        }
    }
    return nullptr;
}

const char* StoreSettingsSD(const tagMenuEntry_t * const , int key)
{
    if (MenuManager::eSelectKey == key) {
//        Store();
        if (nullptr != s_pManager) {
            s_pManager->Message("Store");
        }
    }
    return nullptr;
}

const char* ShowVersion(const tagMenuEntry_t * const , int )
{
    return "Version 1.0\nTest\nLine2";
}



MenuEntry_t subline = { "-\\------", nullptr, {} };
MenuEntry_t emptyline = { "-", nullptr, {} };

MenuEntry_t sub111 = { "+Hour:", nullptr, {} };
MenuEntry_t sub112 = { "+Min:", nullptr, {} };
MenuEntry_t sub113 = { "+Sec:", nullptr, {} };
MenuEntry_t sub114 = { "@35Set", SetTime, {} };
MenuEntry_t sub11 = { "Time", loadTime, {&sub111,&sub112,&sub113,&sub114} };

MenuEntry_t sub121 = { "+Year:", nullptr, {} };
MenuEntry_t sub122 = { "+Mon:", nullptr, {} };
MenuEntry_t sub123 = { "+Day:", nullptr, {} };
MenuEntry_t sub124 = { "@35Set", SetDate, {} };
MenuEntry_t sub12 = { "Date", loadDate, {&sub121,&sub122,&sub123,&sub124} };

MenuEntry_t sub1 = { "Time/Date", nullptr, {&sub11, &sub12} };

MenuEntry_t sub21 = { "#Raw", nullptr, {} };
MenuEntry_t sub22 = { "#GPX", nullptr, {} };
MenuEntry_t sub23 = { "#CSV", nullptr, {} };
MenuEntry_t sub24 = { "#TXT", nullptr, {} };
MenuEntry_t sub2 = { "Files", updateSettings, {&sub21, &sub22, &sub23, &sub24} };

MenuEntry_t sub41 = { "Save to SD", StoreSettingsSD, {} };
MenuEntry_t sub4 = { "|Save", nullptr, {&sub41} };

MenuEntry_t sub51 = { "Restore from SD", RestoreSettingsSD, {} };
MenuEntry_t sub5 = { "Restore", nullptr, {&sub51} };

MenuEntry_t sub61 = { "-@12Version", ShowVersion, {} };
MenuEntry_t sub6 = { "Version", nullptr, {&sub61} };

MenuEntry_t lastsub = { "-@07", ShowTime, {} };

MenuEntry_t main_menu = { "Settings", nullptr, {&sub1, &sub2, &sub4, &sub5, &sub6, &lastsub} };


void TestDataStorage::readSettings()
{
	SelectedFiles = HAL_RTCEx_BKUPRead(&hrtc,RTC_BKP_DR4);
}


void TestDataStorage::writeSettings()
{
	HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR4, SelectedFiles);
}


int32_t TestDataStorage::getData(const MenuEntry_t *const MenuPt)
{
    if (MenuPt == &sub21) {
        return (0U != (SelectedFiles & 1));
    }
    else if (MenuPt == &sub22) {
        return (0U != (SelectedFiles & 2));
    }
    else if (MenuPt == &sub23) {
        return (0U != (SelectedFiles & 4));
    }
    else if (MenuPt == &sub24) {
        return (0U != (SelectedFiles & 8));
    }
    else if (MenuPt == &sub111) {
        return Hours;
    }
    else if (MenuPt == &sub112) {
        return Minutes;
    }
    else if (MenuPt == &sub113) {
        return Seconds;
    }
    else if (MenuPt == &sub121) {
        return Year;
    }
    else if (MenuPt == &sub122) {
        return Month;
    }
    else if (MenuPt == &sub123) {
        return Day;
    }

    return 0;
}

bool TestDataStorage::setData(const MenuEntry_t *const MenuPt, int32_t val )
{
    bool result = false;
    if (MenuPt == &sub21) {
        SelectedFiles &= ~1;
        SelectedFiles |= (val!=0) ? 1:0;
        result = true;
    }
    else if (MenuPt == &sub22) {
        SelectedFiles &= ~2;
        SelectedFiles |= (val!=0) ? 2:0;
        result = true;
    }
    else if (MenuPt == &sub23) {
        SelectedFiles &= ~4;
        SelectedFiles |= (val!=0) ? 4:0;
        result = true;
    }
    else if (MenuPt == &sub24) {
        SelectedFiles &= ~8;
        SelectedFiles |= (val!=0) ? 8:0;
        result = true;
    }
    else if (MenuPt == &sub111) {
        ++Hours;
        if (24U == Hours) {
            Hours = 0U;
        }
        result = true;
    }
    else if (MenuPt == &sub112) {
        ++Minutes;
        if (60U == Minutes) {
            Minutes = 0U;
        }
        result = true;
    }
    else if (MenuPt == &sub113) {
        ++Seconds;
        if (60U == Seconds) {
            Seconds = 0U;
        }
        result = true;
    }
    else if (MenuPt == &sub121) {
        ++Year;
        if (2035U == Year) {
            Year = 2020U;
        }
        result = true;
    }
    else if (MenuPt == &sub122) {
        ++Month;
        if (13U == Month) {
            Month = 1U;
        }
        result = true;
    }
    else if (MenuPt == &sub123) {
        ++Day;
        if (32U == Day) {
            Day = 1U;
        }
        result = true;
    }
    else {
    }
    return result;
}

void TestDataStorage::loadTime()
{
	Cal.getTime(Hours, Minutes, Seconds);
}

void TestDataStorage::loadDate()
{
	Cal.getDate(Year, Month, Day);
}

void TestDataStorage::setDateTime()
{
	Cal.SetDateTime(Year, Month, Day, Hours, Minutes, Seconds);
}

HubsanSettingMenu::HubsanSettingMenu(IDisplayBase *mainwindow)
{
    pManager = new MenuManager(&main_menu, mainwindow, &Td);
}

void HubsanSettingMenu::Show(bool clean)
{
    pManager->Show(clean);
    s_pManager = pManager;
}

void HubsanSettingMenu::Up()
{
    pManager->Up();
}
void HubsanSettingMenu::Down()
{
    pManager->Down();
}

void HubsanSettingMenu::Select()
{
    pManager->Select();
}
void HubsanSettingMenu::Return()
{
    pManager->Return();
}
