
#include "BasicTimer.h"

#include <limits.h>

#if defined(COMPILE_QT)
    #include <QThread>
    #include <QCoreApplication>
#endif

BasicTimer::BasicTimer() : m_EndTime(0U), m_Interval(0U) {
    //#[ operation BasicTimer()
    //#]
}

boolean_t BasicTimer::IsActive() const {
    //#[ operation IsActive() const
    return 0!=m_EndTime;
    //#]
}

TIME_T BasicTimer::Remaining() const
{
    TIME_T Remain=0U;
    TIME_T CurrentTime=GetSysTime();
    if (0U != m_EndTime) {
        if (m_EndTime > CurrentTime) {
            Remain = m_EndTime - CurrentTime;
        }
        else {
            Remain = TIMEMAX_T + (m_EndTime - CurrentTime) +1 ;	// wrap around
        }
    }
    return Remain;
}

void BasicTimer::SetPeriodicTimeout(const TIME_T Interval)
{
    SetSingleTimeout(Interval);
    m_Interval = Interval;
}

void BasicTimer::SetSingleTimeout(const TIME_T Interval)
{
    TIME_T CurrentTime = GetSysTime();
    m_Interval = 0U;
    if 	(0U!= Interval) {
        m_EndTime = CurrentTime + Interval;
    }
    else {
        m_EndTime=0U;		// timer deactivated
    }
}

boolean_t BasicTimer::Timedout() 
{
    boolean_t ret=false;

    if (0U!=m_EndTime) {
        if ((GetSysTime() - m_EndTime) < TIMEMAX_T/2) {
    		ret=true;
            if (0U != m_Interval) {
                SetPeriodicTimeout(m_Interval);
    		}
    	}
    }
    return ret;
}

void Timestamp::Set()
{
    m_Timestamp = GetSysTime();
}

TIME_T Timestamp::TimePasted(bool Update)
{
    TIME_T Now = GetSysTime();
    TIME_T Timestamp = m_Timestamp;
	if (Update) {
        m_Timestamp = Now;
	}
    if (Now >= Timestamp) {
        return Now - Timestamp;
	}
	else {
        return TIMEMAX_T - (Timestamp - Now);	// wrap around
	}
}

void BasicTimer::Delay(TIME_T delay_ms)
{
    BasicTimer D;
    D.SetSingleTimeout(delay_ms);
    while(!D.Timedout()) {
#if defined(QT_VERSION_STR)
        QCoreApplication::processEvents();
        QThread::msleep(1);
#endif
    }
}


#if !defined(VIRTUAL_TIME)
#if defined(QT_CORE_LIB)

    #include <QElapsedTimer>
    class QSysTicks
    {
        friend TIME_T GetSysTime();
        static QElapsedTimer timer;
        static QElapsedTimer::ClockType ct;
        static bool s_started ;
        QSysTicks() { timer.start(); }
    };
    QElapsedTimer QSysTicks::timer;
    QElapsedTimer::ClockType QSysTicks::ct;
    bool QSysTicks::s_started= false;
    TIME_T GetSysTime() {
        if (!QSysTicks::s_started) {
            QSysTicks::timer.start();
            QSysTicks::s_started = true;
        }
        QSysTicks::ct= QElapsedTimer::clockType();
        return QSysTicks::timer.elapsed();
    }


    #if 0 && (defined(_WIN32) || defined(WIN32))
        #include <windows.h>

        static LARGE_INTEGER tick_start={0,0};   // A point in time
        static LARGE_INTEGER ticksPerSecond={0,0};

        TIME_T GetSysTime()
        {

            LARGE_INTEGER tick_now;
            QueryPerformanceCounter(&tick_now);

            if (0==ticksPerSecond.QuadPart) {
                QueryPerformanceFrequency(&ticksPerSecond);
                tick_start=tick_now;
            }
            LARGE_INTEGER tick_diff;
            double time_diff_in_microseconds = 0;
            tick_diff.QuadPart = (tick_now.QuadPart-tick_start.QuadPart);
            time_diff_in_microseconds = (double)tick_diff.QuadPart/(double)ticksPerSecond.QuadPart*1000000;
            return time_diff_in_microseconds/1000;
        }
    #endif

#else

	#ifdef ARDUINO
		#include <Arduino.h>
        TIME_T GetSysTime()
		{
			return millis();
		}

	#else
        volatile TIME_T SysTimerTicks =0;

        TIME_T GetSysTime()
		{
			return SysTimerTicks;
		}
	#endif

	#ifdef USE_HAL_DRIVER

		extern "C" void HAL_SYSTICK_Callback();
		void HAL_SYSTICK_Callback(void)
		{
			SysTimerTicks++;
		}
	#endif
#endif
#endif // !defined(VIRTUAL_TIME)
