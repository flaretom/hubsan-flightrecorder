/*
 * Calendar.h
 *
 *  Created on: 27.10.2019
 *      Author: tom
 */

#ifndef CALENDAR_H_
#define CALENDAR_H_

#include <stdint.h>

class Calendar {

	void jd_to_calendar(uint32_t jd, uint16_t &Year, uint8_t &Month, uint8_t &Day);
	static const uint32_t  JD_2000_JAN_1 = 2451544;
public:
	Calendar();
	bool SetDateTime(uint16_t Year, uint8_t Month, uint8_t Day, uint8_t Hour, uint8_t Minutes, uint8_t Seconds = 0U);
	uint8_t getDateStr(char *szBuffer, uint8_t BufSize, const char terminator);
	void getDate(uint16_t &Year, uint8_t &Month, uint8_t &Day);
	uint8_t getTimeStr(char *szBuffer, uint8_t BufSize, const char terminator);
	void getTime(uint8_t &Hours, uint8_t &Minutes, uint8_t &Seconds);
	static uint32_t gregorian_calendar_to_jd(uint16_t y, uint8_t m, uint8_t d);
};

//uint8_t getTimeStr(char *szBuffer, const uint8_t BufSize, const char terminator);
//uint8_t getDateStr(char *szBuffer, const uint8_t BufSize, const char terminator);

extern Calendar Cal;

#endif /* CALENDAR_H_ */
