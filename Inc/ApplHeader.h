#ifndef APPLHEADER_H
#define APPLHEADER_H

#include <stdint.h>

typedef struct {
	const char Sign[4];
	uint16_t AppID;
	uint16_t AppVersion;
	const uint32_t * AppDefdata;
	const char * const Name;
	const char * const buildDate;
	const char * const buildTime;
	const char * const shortGitRevision;
} AppHeader_t;

#endif // APPLHEADER_H
