/*
 * RawFileGen.h
 *
 *  Created on: 16.10.2019
 *      Author: tom
 */

#ifndef RAWFILEGEN_H_
#define RAWFILEGEN_H_

#include "IFileGen.h"
#include "fatfs.h"

class RawFileGen : public IFileGen {
	FIL rawfile;
	bool isEnabled = false;
public:
	RawFileGen();
	virtual ~RawFileGen();
	bool open(const char *date, const char* time);
	void close();
	bool createRaw(const uint8_t FrameType, const uint8_t * const data, const uint16_t datasize, const char * Timestamp) override;
};

#endif /* RAWFILEGEN_H_ */
