/*
 * TxtFileGen.cpp
 *
 *  Created on: 16.10.2019
 *      Author: tom
 */

#include "TxtFileGen.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct
{
	const char * const ColHeader;
	FlightDataIds DataItem;
} TxtContent_t;

static const TxtContent_t TxtContent[] = {
		{"Ti", eDID_TIME},
		{"Lat", eDID_LAT},
		{"Lon", eDID_LONG},
		{"Ele", eDID_ELEVAT},
		{"Dst", eDID_DISTANCE},
		{"H", eDID_H},
		{"R", eDID_R},
		{"P", eDID_P},
		{"V", eDID_VBAT},
		{"S", eDID_SAT},
		{"t", eDID_THROTTLE},
		{"r", eDID_RUDDER},
		{"p", eDID_PITCH},
		{"y", eDID_YAW},
		{"m", eDID_MARKER},
		{"v", eDID_VIDEO},
		{"p", eDID_PHOTOTGL},
		{"RSI", eDID_RSSI},
		{"vel", eDID_VELOCITY},
		{"mod", eDID_MODE_I},
		{"mot", eDID_MODE_T},
		{nullptr, eDID_NONE}
};

TxtFileGen::TxtFileGen() {
	// TODO Auto-generated constructor stub

}

TxtFileGen::~TxtFileGen() {
	// TODO Auto-generated destructor stub
}

bool TxtFileGen::open(const char *date, const char* time)
{
	if (!isEnabled) {
		FRESULT fresult;
		char buffer[80];
		sprintf(buffer, "H501_%s_%s.txt", date, time);
		fresult = f_open(&outfile, buffer, FA_OPEN_ALWAYS | FA_WRITE); //open file on SD card
		if (FR_OK == fresult) {
			fresult = f_lseek(&outfile, outfile.fsize);         //go to the end of the file

		}
		isEnabled = FR_OK == fresult;
	}
	return isEnabled;
}

void TxtFileGen::close()
{
	isEnabled = false;
	f_close(&outfile);
}

bool TxtFileGen::createEntry(const FlightDataContainer &FDC)
{
	const auto bufsize = 256U;
	char buf[bufsize];
	bool res = false;
	uint16_t idx = 0U;
	if (isEnabled) {
		// prepare output to serial stream
		for (uint8_t i = 0U; nullptr != TxtContent[i].ColHeader ;++i) {
			idx += snprintf(buf + idx , bufsize - idx, " %s:", TxtContent[i].ColHeader);
			idx += FDC.asString(TxtContent[i].DataItem, buf+idx, sizeof(buf) - idx);
		}
		res = true;
	}
	if (res) {
		UINT lenwrt;
		f_write(&outfile, buf, idx, &lenwrt);
		f_sync(&outfile);
	}
	return res;
}


