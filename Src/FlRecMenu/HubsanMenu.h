#ifndef HUBSANSETTINGMENU_H
#define HUBSANSETTINGMENU_H

#include <Menu/MenuManager.h>
#include <Menu/IDataStorage.h>

class HubsanSettingMenu
{
    MenuManager *pManager;
public:
    HubsanSettingMenu(IDisplayBase *mainwindow);

    void Show(bool clean = false);
    void Select();
    void Up();
    void Down();
    void Return();

};

class TestDataStorage : public IDataStorage
{
    uint16_t SelectedFiles = 0;
    uint8_t Hours   = 0;
    uint8_t Minutes = 0;
    uint8_t Seconds = 0;
    uint16_t Year   = 2020;
    uint8_t Month   = 1;
    uint8_t Day     = 1;
public:
    virtual int32_t getData(const MenuEntry_t * const MenuPt);
    virtual bool setData(const MenuEntry_t * const MenuPt, int32_t value);
    void loadTime();
    void loadDate();
    void setDateTime();
    void readSettings();
    void writeSettings();
};


#endif // HUBSANSETTINGMENU_H
