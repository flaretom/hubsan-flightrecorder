/*
 * FlRecConfig.h
 *
 *  Created on: 29.02.2020
 *      Author: tom
 */

#ifndef FLRECCONFIG_FLRECCONFIG_H_
#define FLRECCONFIG_FLRECCONFIG_H_

#include <stdint.h>
#include "fatfs.h"

class FlRecConfig {

	typedef enum {
		cNone = 0,
		cGPXFile = 0x01,
		cTxtFile = 0x02,
		cKMLFile = 0x04,
		cRawFile = 0x08,
		cCsvFile = 0x10
	} OutputFlags_t;

	static const uint8_t NoOfDisplayFields = 10U;
	static const uint8_t SizeOfDisplayField = 64U;
private:
	OutputFlags_t OutputFlags = cNone;
	bool OSD_UseRedButton = true;		// use the red button for switching OSD layouts
	bool ModeIsSwitch = false;			// mode is controlled by button(0) or switch(1)
	bool ModeByJoystick = true;
	bool UseJoystick = true;
	uint8_t OsdChannel = 0U;
	char DisplFields[NoOfDisplayFields][SizeOfDisplayField];
	uint16_t UpdateRate = 100U;

	bool isLineStartsWith(const char * line, const char * pStart);
	void findParameter(const char **pF);
	void configureFiles(const char* parameters, bool &ok);
	bool configureBool(const char* parameters, bool &ok);
	int32_t configureInt(const char* parameters, bool &ok);
	bool configureString(const char* parameters, char *parameterStr, bool &ok);

public:
	FlRecConfig();
	bool load(FIL *file);

	inline bool writeTXTFile() { return 0 != (OutputFlags & OutputFlags_t::cTxtFile); }
	inline bool writeGPXFile() { return 0 != (OutputFlags & OutputFlags_t::cGPXFile); }
	inline bool writeCSVFile() { return 0 != (OutputFlags & OutputFlags_t::cCsvFile); }
	inline bool writeRAWFile() { return 0 != (OutputFlags & OutputFlags_t::cRawFile); }

	uint8_t changeOSDScreen();
	inline bool useRedButtonOSD() const { return OSD_UseRedButton;}
	inline bool isModeSwitch() const { return ModeIsSwitch; }
	inline bool isModeByJoystick() const { return ModeByJoystick; }
	inline bool useJoystick() const { return UseJoystick; }
	const char *getDisplayFieldConfig(const uint8_t DisplayField) const;
	inline uint16_t getUpdateRate() const { return UpdateRate;}
};

extern FlRecConfig GlobalSettings;

#endif /* FLRECCONFIG_FLRECCONFIG_H_ */
