/*
 * Calendar.cpp
 *
 *  Created on: 27.10.2019
 *      Author: tom
 */

#include <Calendar.h>
#include <cstdio>
#include "rtc.h"

Calendar Cal;		// singleton

extern "C" {
	uint32_t           RTC_ReadTimeCounter(RTC_HandleTypeDef *hrtc);
	HAL_StatusTypeDef  RTC_WriteTimeCounter(RTC_HandleTypeDef *hrtc, uint32_t TimeCounter);
}

Calendar::Calendar() {
	// TODO Auto-generated constructor stub

}

uint32_t Calendar::gregorian_calendar_to_jd(uint16_t y, uint8_t m, uint8_t d)
{
    if(m<3) { y--; m+=12; }
    y+=8000;
    long jd = (y*365) +(y/4) -(y/100) +(y/400)
              +(m*153+3)/5-92
              +d-1
          -1200821 ;
    return jd;
}

void Calendar::jd_to_calendar(uint32_t jd, uint16_t &Year, uint8_t &Month, uint8_t &Day)
{
    uint16_t y,m,d;
    for(y=jd/366-4715; gregorian_calendar_to_jd(y+1,1,1)<=jd; ++y);
    for(m=1; gregorian_calendar_to_jd(y,m+1,1)<=jd; ++m);
    for(d=1; gregorian_calendar_to_jd(y,m,d+1)<=jd; ++d);
    Year=y; Month=m; Day=d;
}

uint8_t Calendar::getDateStr(char *szBuffer, uint8_t BufSize, const char terminator)
{
	uint8_t Month, Day;
	uint16_t Year;
	getDate(Year, Month, Day);
	return snprintf(szBuffer, BufSize, "%04i%c%02i%c%02i", Year, terminator, Month, terminator, Day);
}

void Calendar::getDate(uint16_t &Year, uint8_t &Month, uint8_t &Day)
{
	uint32_t timecounter = RTC_ReadTimeCounter(&hrtc);
	uint32_t jd = timecounter/86400;
	jd += JD_2000_JAN_1;
	jd_to_calendar(jd, Year, Month, Day);
}

uint8_t Calendar::getTimeStr(char *szBuffer, uint8_t BufSize, const char terminator)
{
	uint8_t Hours;
	uint8_t Minutes;
	uint8_t Seconds;
	getTime(Hours, Minutes, Seconds);
	return snprintf(szBuffer, BufSize, "%02i%c%02i%c%02i", Hours, terminator, Minutes, terminator, Seconds);
}

void Calendar::getTime(uint8_t &Hours, uint8_t &Minutes, uint8_t &Seconds)
{
	uint32_t timecounter = RTC_ReadTimeCounter(&hrtc);
	uint32_t secthisday = timecounter%86400;
	Hours = secthisday/3600;
	Minutes = (secthisday % 3600)/60;
	Seconds = secthisday % 60;
}

bool Calendar::SetDateTime(uint16_t Year, uint8_t Month, uint8_t Day, uint8_t Hour, uint8_t Minutes, uint8_t Seconds)
{
	bool res = true;

	uint32_t timecounter = Hour*3600L + Minutes*60L + Seconds;
	uint32_t jd = gregorian_calendar_to_jd(Year, Month, Day);
	jd -= JD_2000_JAN_1;
	timecounter += jd * 86400;

	const uint32_t settime = HAL_RTCEx_BKUPRead(&hrtc,RTC_BKP_DR2) + (HAL_RTCEx_BKUPRead(&hrtc,RTC_BKP_DR3)<<16U);
	if (settime != timecounter) {	// time already set
		RTC_WriteTimeCounter(&hrtc, timecounter);
		HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR1, 0x32F2);
		HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR2, timecounter & 0xFFFFU);
		HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR3, timecounter >> 16U);
	}
	return res;
}

//uint8_t getDateStr(char *szBuffer, uint8_t BufSize, const char terminator)
//{
//	RTC_DateTypeDef sdate = {0};
//	HAL_RTC_GetDate(&hrtc, &sdate, RTC_FORMAT_BIN);
//	uint16_t Year = sdate.Year + 2000;
//	return snprintf(szBuffer, BufSize, "%04i%c%02i%c%02i", Year, terminator, sdate.Month, terminator, sdate.Date);
//}
//
//uint8_t getTimeStr(char *szBuffer, uint8_t BufSize, const char terminator)
//{
//	RTC_TimeTypeDef stime = {0};
//	HAL_RTC_GetTime(&hrtc, &stime, RTC_FORMAT_BIN);
//	return snprintf(szBuffer, BufSize, "%02i%c%02i%c%02i", stime.Hours, terminator, stime.Minutes, terminator, stime.Seconds);
//}

