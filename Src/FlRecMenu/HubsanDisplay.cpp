/*
 * HubsanDisplay.cpp
 *
 *  Created on: 17.01.2020
 *      Author: tom
 */

#include <FlRecMenu/HubsanDisplay.h>
#include <Utilities/BasicTimer.h>

NanoEngine<TILE_128x64_MONO> engine;

HubsanDisplay::HubsanDisplay() {
	// TODO Auto-generated constructor stub

}

HubsanDisplay::~HubsanDisplay() {
	// TODO Auto-generated destructor stub
}

const unsigned char ReturnBmp [16] = {
		0x10, 0x38, 0x7C, 0xFE, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x38, 0x3C, 0x3E, 0x1E, 0x1E, 0x06
};
const unsigned char OkBmp [] = {
		0x00, 0x00, 0x00, 0x30, 0x70, 0xE0, 0xE0, 0x70, 0x38, 0x1C, 0x0E, 0x06, 0x00, 0x00, 0x00, 0x00
};

const unsigned char CheckBox_off [] = {
		0x00, 0x00, 0x00, 0xFF, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0xFF, 0x00, 0x00, 0x00, 0x00
};

const unsigned char CheckBox_on [] = {
		0x00, 0x00, 0x00, 0xFF, 0x81, 0xBD, 0xBD, 0xBD, 0xBD, 0xBD, 0x81, 0xFF, 0x00, 0x00, 0x00, 0x00
};

HubsanDisplay::BitmapList_t HubsanDisplay::BitmapList[2] = {
		{ 16,8, ReturnBmp },
		{ 16,8, OkBmp }
};

void HubsanDisplay::printFixed( unsigned xpos, unsigned ypos, const char * const text, IDisplayBase::IDisplayFlag_t flags)
{
	switch (flags) {
		case IDisplayBase::eNormal:
			engine.canvas.printFixed( xpos*32, ypos*8, text, STYLE_NORMAL);
//			ssd1306_printFixed(xpos*32, ypos*8, text, STYLE_NORMAL);
			break;
		case IDisplayBase::eTitle:
			engine.canvas.setColor(0);
			engine.canvas.printFixed( xpos*32, ypos*8, text, STYLE_BOLD);
			engine.canvas.setColor(1);
//			ssd1306_printFixed(xpos*32, ypos*8, text, STYLE_BOLD);
			break;
		case IDisplayBase::eSelected:
			engine.canvas.printFixed( xpos*32, ypos*8, text, STYLE_BOLD);
//			ssd1306_printFixed(xpos*32, ypos*8, text, STYLE_BOLD);
			break;
		case IDisplayBase::eMessage:
			engine.canvas.printFixed( xpos*32, 20, text, STYLE_BOLD);
//			ssd1306_printFixed(xpos*32, ypos*8, text, STYLE_BOLD);
			BasicTimer::Delay(2000);
			break;
	}
}

bool HubsanDisplay::drawBitmap( unsigned xpos, unsigned ypos, int BitMapId, bool inverse)
{
	const uint8_t w = BitmapList[BitMapId].width;
	const uint8_t h = BitmapList[BitMapId].height;
	const uint8_t * const bitmap = BitmapList[BitMapId].data;
	if (inverse) {
		engine.canvas.drawBitmap1(xpos, ypos, w, h, bitmap);
	}
	else {
		engine.canvas.setColor(0);
		engine.canvas.drawBitmap1(xpos, ypos, w, h, bitmap);
		engine.canvas.setColor(1);
	}
	return true;
}

void HubsanDisplay::clear(bool clean)
{
	if (clean) {
		initialize();
	}
	engine.canvas.clear();
}

void HubsanDisplay::update()
{
	engine.canvas.blt(0, 0);
}


void HubsanDisplay::initialize()
{
	ssd1306_setFixedFont(ssd1306xled_font6x8);
	ssd1306_128x64_i2c_init();
	ssd1306_setFixedFont(ssd1306xled_font6x8);
	ssd1306_positiveMode();
	ssd1306_clearScreen();
}
