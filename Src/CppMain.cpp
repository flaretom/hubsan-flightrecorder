/*
 * CppMain.cpp
 *
 *  Created on: 03.10.2019
 *      Author: tom
 */

#define TESTDATA	0

#include "version.h"

#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "main.h"
#include "gpio.h"
#include "usart.h"
#include "rtc.h"
#include "dma.h"
#include "spi.h"
#include "stm32f1xx_hal.h"
#include "usart.h"
#include "Utilities/BasicTimer.h"
#include "fatfs.h"

#include "ApplHeader.h"

#include "Output/TxtFileGen.h"
#include "Output/RawFileGen.h"
#include "Output/GpxFileGen.h"
#include "Output/CsvFileGen.h"
#include <OSD/MSPCommandHandler.h>

#include "DebouncedSwitch.h"

#include <Src/Calendar.h>
#include <Src/FlightData.h>
#include <FlRecConfig/FlRecConfig.h>
#include <FlightDisplay/FlightDisplay.h>
#include <FlRecMenu/HubsanDisplay.h>
#include <FlRecMenu/HubsanMenu.h>

extern I2C_HandleTypeDef hi2c1;


// --------------  connection to bootloader / version handling
const char * const buildDate = __DATE__;
const char * const buildTime = __TIME__;
const char * const shortGitRevision = __REVISION__;

extern uint32_t __appDefdata;

const char * const AppName = "HubsanFlighRec";

AppHeader_t AppHeader __attribute__((used)) __attribute__ ((section (".appheader"))) = {	//
//const AppHeader_t AppHeader = {
	{ 'A', 'p', 'p', 'l' },
	0x0001,
	0x0106,
	(&__appDefdata)+1,
	AppName,
	buildDate,
	buildTime,
	shortGitRevision
} ;

uint32_t AppData __attribute__((used)) __attribute__ ((section (".AppDefArea"))) = 0xDEAD;

// --------------  internal prototypes

void HandleButtons();
bool InitializeDisplay();
void HandleReceivedFrames();
void CheckSDCard();
void OpenLogFiles();
void CloseLogFiles();
bool WriteLogFiles();

// --------------  c-linkage

extern "C" {
	void CppMain();

	void MX_USART1_UART_Init();
	void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi);
	void HAL_SPI_RxHalfCpltCallback(SPI_HandleTypeDef *hspi);
	void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
	void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart);

	uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);
	void USB_Received(uint8_t* Buf, uint32_t *Len);
	HAL_StatusTypeDef UART_Receive_IT(UART_HandleTypeDef *huart);
	HAL_StatusTypeDef UART_Transmit_IT(UART_HandleTypeDef *huart);

	void USBD_CDC_TransferFinished();
}

// --------------  data related to SPI sniffing
// receiving data from A7105
bool isDMAinitialized = false;
const uint8_t cFrameSize = 17U;
uint8_t FrameBuffer[2 * cFrameSize];	// command + receive frame
uint8_t Telemetry1[cFrameSize];
uint8_t Telemetry2[cFrameSize];
uint8_t Control[cFrameSize];

// counters for received frames
uint16_t Cnt_e8 = 0;
uint16_t Cnt_e7 = 0;
uint16_t Cnt_05 = 0;

// counters for handled frames
uint16_t CntP_05 = 0;
uint16_t CntP_e7 = 0;
uint16_t CntP_e8 = 0;

// // -------------- SD card/ filesystem

// SD card logging
FATFS g_sFatFs;
uint16_t g_sFatSectorSize = 0;
bool s_bUseSDCard = false;

// --------------  file generators
TxtFileGen TxtLog;
CsvFileGen CsvLog;
RawFileGen RawLog;
GpxFileGen GpxLog;

extern TestDataStorage Td;

// -------------- OLED
bool bUseOLED = true;

// --------------  OSD
MSPCommandHandler MSP_Uart(&huart3);
MSPCommandHandler MSP_Usb;

// --------------  Buttons
const uint16_t LONGPRESS = 500;		// press LONGPRESS ms for direct shutter controll
const uint16_t SHORTPRESS = 20;

DebouncedSwitch ButtonGreen(2, INPUT_PULLUP, LOW, SHORTPRESS, LONGPRESS, 400, 10);
DebouncedSwitch ButtonBlue(1, INPUT_PULLUP, LOW, SHORTPRESS, LONGPRESS, 400, 10);
DebouncedSwitch ButtonRed(0, INPUT_PULLUP, LOW, SHORTPRESS, LONGPRESS, 400, 10);

extern volatile TIME_T SysTimerTicks;

uint8_t UartRecBuffer[256];

bool LogsCreated = false;
bool LogsActive = false;

HubsanDisplay Display;
HubsanSettingMenu SettingMenuManager(&Display);
FlightDisplay HubsanFlightDisplay(&Display);

char s_DateStr[20];
char s_Timestamp[20];


// --------------------------------------------------------------------------

void USB_Received(uint8_t* Buf, uint32_t *pLen)
{
	uint32_t Len = 0U;
	if (pLen) {
		Len = *pLen;
	}
	MSP_Usb.ReceiveUsb(Buf, Len);
}

HAL_StatusTypeDef UART_Receive_IT(UART_HandleTypeDef *huart)
{
	MSP_Uart.ReceiveSerial(huart->Instance->DR);
	return HAL_OK;
}

void USBD_CDC_TransferFinished()
{
	MSP_Usb.USBSend();
}

HAL_StatusTypeDef UART_Transmit_IT(UART_HandleTypeDef *huart)
{
	uint8_t byte;
	if (MSP_Uart.SendSerial(byte)) {
		huart->Instance->DR = byte;
		__HAL_UART_ENABLE_IT(huart, UART_IT_TXE);
	}
	else {
	    __HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
	}
	return HAL_OK;
}

// --------------------------------------------------------------------------

extern"C" uint32_t get_fattime(void)
{
    uint8_t Hours   = 0;
    uint8_t Minutes = 0;
    uint8_t Seconds = 0;
    uint16_t Year   = 2020;
    uint8_t Month   = 1;
    uint8_t Day     = 1;
	Cal.getDate(Year, Month, Day);
	Cal.getTime(Hours, Minutes, Seconds);


	uint32_t res =  (((uint32_t)Year - 1980) << 25)
			| ((uint32_t)Month << 21)
			| ((uint32_t)Day << 16)
			| (uint32_t)(Hours << 11)
			| (uint32_t)(Minutes << 5)
			| (uint32_t)(Seconds >> 1);

	return res;
}

// --------------------------------------------------------------------------
void SendMessage(const char * str)
{
	HAL_UART_Transmit_IT(&huart3, const_cast<uint8_t *>(reinterpret_cast<const uint8_t*>(str)), strlen(str));
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	if (&huart3 == huart) {
		MSP_Uart.Reset();
	}
}


// receiving packages from A7105
void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi)
{
#if 2 == TESTDATA
	FDC.InjectTestData(false);
#else
	if (0x45 == FrameBuffer[0]) {	// telemetry data
		if (0xE7 == FrameBuffer[1]) {		// pose
			memcpy(Telemetry1, FrameBuffer+1, 16U);
			Cnt_e7=CntP_e7 + 1;
			FDC.HandleReceivedFrameE7(Telemetry1);
		}
		if (0xE8 == FrameBuffer[1]) {	// GPS
			memcpy(Telemetry2, FrameBuffer+1, 16U);
//			Cnt_e8=CntP_e8 + 1;
			FDC.HandleReceivedFrameE8(Telemetry2);
		}
	}
	if (0x05 == FrameBuffer[0]) {	// control data
		memcpy(Control, FrameBuffer+1, 16U);
		Cnt_05 = CntP_05 + 1;
		FDC.HandleReceivedFrame05(Control);
	}
#endif
}

void HAL_SPI_RxHalfCpltCallback(SPI_HandleTypeDef *hspi)
{
//	if (0x45 == FrameBuffer[0]) {	// telemetry data
//		gotFrame = true;
//	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_PIN_4 == GPIO_Pin && isDMAinitialized) {		// SPI-SS gone high --> end of transmission before buffer filled
		HAL_SPI_DMAStop(&hspi1);
		HAL_SPI_Receive_DMA(&hspi1, FrameBuffer, cFrameSize);	// restart
	}
}

// =============================================================================

void CppMain()
{
	BasicTimer LoggingTimer;
	BasicTimer BatCheck;

	LoggingTimer.SetSingleTimeout(100U);
	BatCheck.SetPeriodicTimeout(30000U);		// measure every 30s

	ButtonGreen.init();
	ButtonRed.init();
	ButtonBlue.init();

    // start of A7105 receiving
	HAL_SPI_Receive_DMA(&hspi1, FrameBuffer, cFrameSize);
	isDMAinitialized = true;

	#if 1 == TESTDATA
		FDC.InjectTestData(true);
	#endif


	bUseOLED = InitializeDisplay();

	MSP_Uart.Init(FDC.get(),&MSP_Usb);
	MSP_Usb.Init(FDC.get(), &MSP_Uart);

	Td.readSettings();

	// main loop
	while (true) {

		bool LowBatWarning = false;

		CheckSDCard();
		HandleButtons();

		MSP_Usb.Cyclic();

		HandleReceivedFrames();
		if (BatCheck.Timedout()) {

		}

		// logging ouput
		if (LoggingTimer.Timedout()) {
			LowBatWarning = WriteLogFiles();
			LoggingTimer.SetSingleTimeout(GlobalSettings.getUpdateRate());
		}
	    if (bUseOLED ) {
	    	Cal.getDateStr(s_DateStr, sizeof(s_DateStr), '-');
	    	Cal.getTimeStr(s_Timestamp, sizeof(s_Timestamp), '-');
	    	if (LogsActive) {
	    		HubsanFlightDisplay.update();
	    	}
	    	else {
	    		SettingMenuManager.Show();
	    	}
	    }
		else {
			//
			if(LowBatWarning) {
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);		// green on
			}
			if (LogsActive) {
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
			}
			else {
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
			}
		}
	}
}

void CheckSDCard()
{
	FRESULT fresult;
	// prepare SD card
	if (!s_bUseSDCard) {
		if (HAL_GPIO_ReadPin(SD_DET_GPIO_Port, SD_DET_Pin) == GPIO_PIN_RESET) {		// card present?
			Display.clear(false);
			Display.printFixed(1, 2, "SD detected"); Display.update();
			BasicTimer::Delay(200);		// some debounce time
			fresult = f_mount(&g_sFatFs, "0:", 0);        //mount SD card
			if (FR_OK == fresult) {
				disk_ioctl(0,GET_SECTOR_SIZE, &g_sFatSectorSize);

				FIL config;
				fresult = f_open(&config, "FlightRec.ini" , FA_OPEN_ALWAYS | FA_READ); //open file on SD card
				if (FR_OK == fresult) {
					Display.printFixed(0, 3, "FlightRec.ini found");
					Display.update();
					s_bUseSDCard = true;
					if (GlobalSettings.load(&config)) {
						Display.printFixed(1, 4, "Config ok");
					}
					else {
						Display.printFixed(1, 4, "Config error");
					}
					Display.update();
					f_close(&config);
					BasicTimer::Delay(500);
				}
				else {
					Display.printFixed(0, 3, "Read Config failed");
					Display.update();
					BasicTimer::Delay(1000);
				}
				//GlobalSettings.OutputFlags = static_cast <OutputFlags_t> (OutputFlags_t::cTxtFile + OutputFlags_t::cGPXFile + OutputFlags_t::cRawFile + OutputFlags_t::cCsvFile);
			}
		}
	}
	else {
		s_bUseSDCard = HAL_GPIO_ReadPin(SD_DET_GPIO_Port, SD_DET_Pin) == GPIO_PIN_RESET;
		if (!s_bUseSDCard) {
			Display.printFixed(1, 3, "SD card removed");
			Display.update();
			BasicTimer::Delay(1000);
		}
	}
}

void OpenLogFiles()
{
	char TimeStr[20];
	char DateStr[20];
	uint8_t line = 2U;
	bool ok;
	bool AllOk = true;

	Display.clear(false);
	Display.printFixed(0,0,"Prepare Logfiles");
	Display.update();

	Cal.getTimeStr(TimeStr, sizeof(TimeStr), '-');
	Cal.getDateStr(DateStr, sizeof(DateStr), '-');
	if (GlobalSettings.writeTXTFile()) {
		Display.printFixed(0, line, "open Txt "); Display.update();
		ok = TxtLog.open(DateStr, TimeStr);
		Display.printFixed(2, line, ok?"OK":"fail"); Display.update();
		++line;
		AllOk &= ok;
	}
	if (GlobalSettings.writeCSVFile()) {
		Display.printFixed(0, line, "open CSV "); Display.update();
		ok = CsvLog.open(DateStr, TimeStr);
		Display.printFixed(2, line, ok?"OK":"fail"); Display.update();
		++line;
		AllOk &= ok;
	}
	if (GlobalSettings.writeRAWFile()) {
		Display.printFixed(0, line, "open Raw "); Display.update();
		ok = RawLog.open(DateStr, TimeStr);
		Display.printFixed(2, line, ok?"OK":"fail"); Display.update();
		++line;
		AllOk &= ok;
	}
	if (GlobalSettings.writeGPXFile()) {
		Display.printFixed(0, line, "open GPX "); Display.update();
		ok = GpxLog.open(DateStr, TimeStr);
		Display.printFixed(2, line, ok?"OK":"fail"); Display.update();
		++line;
		AllOk &= ok;
	}
	BasicTimer::Delay(AllOk? 500: 1000);
}

bool WriteLogFiles()
{
	char Timestamp[20];
	Cal.getTimeStr(Timestamp, sizeof(Timestamp), ':');
	FDC.updateTimeDate();

	if (s_bUseSDCard) {
		if (GlobalSettings.writeRAWFile()) {
			// prepare output to file (raw)
			if (FDC.writeRAWEntry(cNavData)) {
				RawLog.createRaw(cNavData, Telemetry2, 16U, Timestamp);
			}
			if (FDC.writeRAWEntry(cTeleData)) {
				RawLog.createRaw(cTeleData, Telemetry1, 16U, Timestamp);
			}
			if (FDC.writeRAWEntry(cControl)) {
				RawLog.createRaw(cControl, Control, 16U, Timestamp);
			}
		}
		// prepare output to file (txt)
		if (GlobalSettings.writeTXTFile()) {
			TxtLog.createEntry(FDC);
		}
		// prepare output to file (txt)
		if (GlobalSettings.writeGPXFile()) {
			GpxLog.createEntry(FDC);
		}
		// prepare output to file (csv)
		if (GlobalSettings.writeCSVFile()) {
			CsvLog.createEntry(FDC);
		}
	}
	FDC.clearFrames();
	FDC.clearMarker();
	bool ok;
	return FDC.getU(eDID_VBAT, ok) < 72;
}

void CloseLogFiles()
{
	TxtLog.close();
	RawLog.close();
	GpxLog.close();
	CsvLog.close();
}

void HandleButtons()
{
	// update buttons
	ButtonGreen.poll();
	ButtonBlue.poll();
	ButtonRed.poll();

	bool SettingMode = false;
	static bool SimulateToggle = true;

	if (GlobalSettings.isModeByJoystick()) {
		if (FDC.isStartStopCondition()) {
			SimulateToggle = !SimulateToggle;
		}
		SettingMode = SimulateToggle;
	}
	if (GlobalSettings.isModeSwitch()) {
		SettingMode = ButtonBlue.on();
	}
	else {
		if (ButtonBlue.longPress()) {
			SimulateToggle = !SimulateToggle;
		}
		SettingMode = SimulateToggle;
	}
	if (SettingMode) {
		if (GlobalSettings.useJoystick()) {
			if (FDC.Down()) {
				SettingMenuManager.Down();
			}
			if (FDC.Right()) {
				SettingMenuManager.Select();
			}
			if (FDC.Up()) {
				SettingMenuManager.Up();
			}
			if (FDC.Left()) {
				SettingMenuManager.Return();
			}
		}
		else {
			if (ButtonGreen.pushed()) {
				SettingMenuManager.Down();
			}
			if (ButtonRed.pushed()) {
				SettingMenuManager.Select();
			}
		}
		if (LogsCreated) {
			LogsCreated = false;
			CloseLogFiles();
			Display.clear(true);
		}
		LogsActive = false;
	}
	else {
		if (!LogsCreated && s_bUseSDCard) {
			LogsCreated = true;
			FDC.startRecording();		// store start time
			OpenLogFiles();
			Display.clear(true);
		}
		if (ButtonGreen.pushed()) {
			FDC.updateMarker(0x01);
		}
		if (ButtonGreen.doubleClick()) {
			FDC.updateMarker(0x04);
		}
		if (ButtonGreen.longPress()) {
			FDC.updateMarker(0x10);
		}
		if (GlobalSettings.useRedButtonOSD()) {
			if (ButtonRed.pushed()) {
				uint8_t OsdSwitch = GlobalSettings.changeOSDScreen();
				MSP_Uart.setOSDLayout(OsdSwitch);
				MSP_Usb.setOSDLayout(OsdSwitch);
			}
		}
		else {
			if (ButtonRed.pushed()) {
				FDC.updateMarker(0x02);
			}
			if (ButtonRed.doubleClick()) {
				FDC.updateMarker(0x08);
			}
			if (ButtonRed.longPress()) {
				FDC.updateMarker(0x20);
			}
		}
		LogsActive = true;
	}
}

bool InitializeDisplay()
{
	BasicTimer::Delay(200);

	const bool bUseOLED = is_ssd1306_present();
	if (bUseOLED) {
		char szBuffer[80];
		Display.initialize();
		Display.printFixed(0, 0, AppHeader.Name);
		sprintf(szBuffer,"Ver: %x", AppHeader.AppVersion);
		Display.printFixed(0, 8, szBuffer);
		Display.printFixed(0, 40, AppHeader.buildDate);
		Display.printFixed(0, 48, AppHeader.buildTime);
		sprintf(szBuffer,"Git: %s", AppHeader.shortGitRevision);
		Display.printFixed(0, 56, szBuffer);
		BasicTimer::Delay(2000);
		Display.clear(true);
	}
	else {
		GPIO_InitTypeDef GPIO_InitStruct = {0};

		HAL_I2C_MspDeInit(&hi2c1);
		GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
	}
	return bUseOLED;
}


void HandleReceivedFrames()
{
	#if 1 == TESTDATA
		FDC.InjectTestData(false);
	#endif
#if 0
		// handle packages from A7105
		if (CntP_e8 != Cnt_e8) {
			++CntP_e8;
			FDC.HandleReceivedFrameE8(Telemetry2);
		}
		if (CntP_e7 != Cnt_e7) {
			++CntP_e7;
			FDC.HandleReceivedFrameE7(Telemetry1);
		}
		if (CntP_05 != Cnt_05 && 0x20 == Control[0] ) {
			++CntP_05;
			FDC.HandleReceivedFrame05(Control);
		}
#endif
}

// support for DebouncedSwitch
void pinMode(uint8_t pin, uint8_t mode)
{
	// done in main.c
}

bool digitalRead(uint8_t pin)
{
	switch (pin) {
	case 0:
		return HAL_GPIO_ReadPin(BTN_GR_GPIO_Port, BTN_GR_Pin);
	case 1:
		return HAL_GPIO_ReadPin(BTN_BL_GPIO_Port, BTN_BL_Pin);
	case 2:
		return HAL_GPIO_ReadPin(BTN_RD_GPIO_Port, BTN_RD_Pin);
	default:
		// ignore
		break;
	}
	return 0;
}

inline uint32_t millis(void)       // millis()
{
    return GetSysTime();
}


extern "C" {
	void  ssd1306_spiDataMode(uint8_t mode);
}
void ssd1306_spiDataMode(uint8_t mode) {}
