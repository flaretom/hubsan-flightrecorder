/*
 * MenuManager.h
 *
 *  Created on: 16.01.2020
 *      Author: tom
 */

#ifndef MENUMANAGER_H_
#define MENUMANAGER_H_

#include <stdint.h>
#include "MenuEntry.h"

class IDisplayBase;
class IDataStorage;

class MenuManager {
    unsigned m_MenuLevel = 0U;
    int m_Selected = 0;
    unsigned m_Column = 0U;
    unsigned m_Row = 0U;
    unsigned m_StartEntry = 0U;
    const MenuEntry_t * m_MenuStack[8] = {nullptr};
    IDisplayBase *m_pDisplay = nullptr;
    IDataStorage *m_Storage = nullptr;

    uint32_t ShowSpecialField(const char * text,const MenuEntry_t * const MenuPt);
    bool UpdateSpecialField(const char * text,const MenuEntry_t * const MenuPt);
    const char * FilterSpecialField(const char * text);
public:

    enum {
    	eUpdateDisplay,
		eSelectKey,
		eUpKey,
		eDownKey,
		eReturnKey
    };

    MenuManager(const MenuEntry_t * const MainMenu, IDisplayBase *Display, IDataStorage *Storage ) : m_pDisplay(Display), m_Storage(Storage) { m_MenuStack[0] = MainMenu;}
	virtual ~MenuManager();
    void Message(const char * const text, uint32_t DisplTime = 2000U );
    void Show(bool clean = false);
    void Select();
    void Up();
    void Down();
    void Return();
};

#endif /* MENUMANAGER_H_ */
