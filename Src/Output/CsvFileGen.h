/*
 * TxtFileGen.h
 *
 *  Created on: 16.10.2019
 *      Author: tom
 */

#ifndef CSVFILEGEN_H_
#define CSVFILEGEN_H_

#include "IFileGen.h"
#include <ff.h>

class CsvFileGen : public IFileGen {
	FIL outfile;
	bool isEnabled = false;
public:
	CsvFileGen();
	virtual ~CsvFileGen();
	bool open(const char *date, const char* time);
	void close();
	bool createEntry(const FlightDataContainer &FDC);
};

#endif /* TXTFILEGEN_H_ */
