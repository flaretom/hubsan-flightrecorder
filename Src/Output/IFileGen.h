/*
 * IFileGen.h
 *
 *  Created on: 16.10.2019
 *      Author: tom
 */

#ifndef IFILEGEN_H_
#define IFILEGEN_H_

#include <FlightData.h>

class IFileGen {
public:
	IFileGen();
	virtual ~IFileGen();
	virtual bool open(const char *date, const char* time) = 0;
	virtual void close() = 0;
	virtual bool createEntry(const FlightDataContainer &FDC) { return false; };
	virtual bool createRaw(const uint8_t FrameType, const uint8_t * const data, const uint16_t datasize, const char * Timestamp) { return false; };
};

#endif /* IFILEGEN_H_ */
