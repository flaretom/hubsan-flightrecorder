/*
 * FlightDisplay.h
 *
 *  Created on: 29.02.2020
 *      Author: tom
 */

#ifndef FLIGHTDISPLAY_FLIGHTDISPLAY_H_
#define FLIGHTDISPLAY_FLIGHTDISPLAY_H_

#include <stdint.h>

class IDisplayBase;

class FlightDisplay {
    IDisplayBase *m_pDisplay = nullptr;
	const char * FillDisplayField(uint8_t DataItem);
	const char *findEndOfField(const char *pStr);
	void ParseDisplayConfig(const char *CfgStr);
public:
	FlightDisplay(IDisplayBase *pDisplay);
	bool update();
};

#endif /* FLIGHTDISPLAY_FLIGHTDISPLAY_H_ */
