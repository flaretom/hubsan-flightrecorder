/*
 * RawFileGen.cpp
 *
 *  Created on: 16.10.2019
 *      Author: tom
 */

#include "RawFileGen.h"
#include <cstdio>
#include <stdlib.h>

RawFileGen::RawFileGen() {
	// TODO Auto-generated constructor stub

}

RawFileGen::~RawFileGen() {
	// TODO Auto-generated destructor stub
}

bool RawFileGen::open(const char *date, const char* time)
{
	if (!isEnabled) {
		FRESULT fresult;
		char buffer[80];
		sprintf(buffer, "H501_%s_%s.raw", date, time);
		fresult = f_open(&rawfile, buffer, FA_OPEN_ALWAYS | FA_WRITE); //open file on SD card
		if (FR_OK == fresult) {
			fresult = f_lseek(&rawfile, rawfile.fsize);         //go to the end of the file
		}
		isEnabled = FR_OK == fresult;
	}
	return isEnabled;
}

void RawFileGen::close()
{
	isEnabled = false;
	f_close(&rawfile);
}

bool RawFileGen::createRaw(const uint8_t FrameType, const uint8_t * const data, const uint16_t datasize, const char * Timestamp)
{
	const auto bufsize = 256U;
	char buf[bufsize];
	const uint8_t *pBuf = data;

	if (nullptr != pBuf && isEnabled) {
		uint8_t idx = 0U;
		idx += snprintf(buf + idx , bufsize - idx, "%s;",Timestamp);
		idx += snprintf(buf + idx , bufsize - idx, "%02x;", FrameType);
		for (uint8_t i = 0U ; datasize > i; ++i) {
			idx += snprintf(buf + idx , bufsize - idx, "%02x;", *pBuf);
			++pBuf;
		}
		idx += snprintf(buf + idx , bufsize - idx, "\n");
		UINT lenwrt;
		f_write(&rawfile, buf, idx, &lenwrt);
		f_sync(&rawfile);
	}

	return false;
}
