/*
 * CsvFileGen.cpp
 *
 *  Created on: 16.10.2019
 *      Author: tom
 */

#include "CsvFileGen.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

CsvFileGen::CsvFileGen() {
	// TODO Auto-generated constructor stub

}

CsvFileGen::~CsvFileGen() {
	// TODO Auto-generated destructor stub
}

typedef struct
{
	const char * const ColHeader;
	FlightDataIds DataItem;
} CSVContent_t;

static const CSVContent_t CSVContent[] = {
		{"Time", eDID_TIME},
		{"frames", eDID_VALIDFRAMES},
		{"Lat", eDID_LAT},
		{"Lon", eDID_LONG},
		{"Elev", eDID_ELEVAT},
		{"Dist", eDID_DISTANCE},
		{"Heading", eDID_H},
		{"Roll", eDID_R},
		{"Pitch", eDID_P},
		{"Vbat", eDID_VBAT},
		{"Sats", eDID_SAT},
		{"throttle", eDID_THROTTLE},
		{"rudder", eDID_RUDDER},
		{"pitch", eDID_PITCH},
		{"yaw", eDID_YAW},
		{"marker", eDID_MARKER},
		{"video", eDID_VIDEO},
		{"photo", eDID_PHOTOTGL},
		{"RSSI", eDID_RSSI},
		{"velocity", eDID_VELOCITY},
		{"mode", eDID_MODE_I},
		{"modt", eDID_MODE_T},
		{nullptr, eDID_NONE}
};

bool CsvFileGen::open(const char *date, const char* time)
{
	if (!isEnabled) {
		FRESULT fresult;
		UINT lenwrt;
		char buffer[256];
		sprintf(buffer, "H501_%s_%s.csv", date, time);
		fresult = f_open(&outfile, buffer, FA_OPEN_ALWAYS | FA_WRITE); //open file on SD card
		if (FR_OK == fresult) {
			uint16_t idx = 0U;
			fresult = f_lseek(&outfile, outfile.fsize);         //go to the end of the file
			for (uint8_t i = 0U; nullptr != CSVContent[i].ColHeader ;++i) {
				idx += sprintf(buffer+idx, "%s",CSVContent[i].ColHeader);
				if (nullptr != CSVContent[i+1].ColHeader) {
					buffer[idx]=';';
					++idx;
				}
			}
			buffer[idx]='\n';
			++idx;
			fresult = f_write(&outfile, buffer, strlen(buffer), &lenwrt);
			f_sync(&outfile);
		}
		isEnabled = FR_OK == fresult;
	}
	return isEnabled;
}

void CsvFileGen::close()
{
	isEnabled = false;
	f_close(&outfile);
}

bool CsvFileGen::createEntry(const FlightDataContainer &FDC)
{
	const uint16_t bufsize = 256U;
	char buf[bufsize];
	bool res = false;
	uint16_t idx = 0U;
	bool ok = true;
	if (isEnabled && FDC.getU(eDID_VALIDFRAMES, ok)) {
		for (uint8_t i = 0U; nullptr != CSVContent[i].ColHeader ;++i) {
			idx += FDC.asString(CSVContent[i].DataItem, buf+idx, bufsize - idx);
			if (nullptr != CSVContent[i+1].ColHeader) {
				buf[idx]=';';
				++idx;
			}
		}
		buf[idx]='\n';
		++idx;
		res = true;
	}
	if (res) {
		UINT lenwrt;
		f_write(&outfile, buf, idx, &lenwrt);
		f_sync(&outfile);
	}
	return res;
}



