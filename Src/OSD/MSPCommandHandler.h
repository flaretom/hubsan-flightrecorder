/*
 * MSPCommandHandler.h
 *
 *  Created on: 26.10.2019
 *      Author: tom
 */

#ifndef OSD_MSPCOMMANDHANDLER_H_
#define OSD_MSPCOMMANDHANDLER_H_

#include <Src/FlightData.h>
#include "usart.h"
#include "MSP_Multiwii.h"
#include <Utilities/BigPipe.h>

#define GIT_SHORT_REVISION_LENGTH   7 // lower case hexadecimal digits.
extern const char* const shortGitRevision;
#define BUILD_DATE_LENGTH 11
extern const char* const buildDate;  // "MMM DD YYYY" MMM = Jan/Feb/...
#define BUILD_TIME_LENGTH 8
extern const char* const buildTime;  // "HH:MM:SS"


class MSPCommandHandler {

public:
	// taken from msp_box.h (Cleanflight and Betaflight)
	typedef struct box_s {
	    const uint8_t boxId;            // see boxId_e
	    const char *boxName;            // GUI-readable box name
	    const uint8_t permanentId;      // permanent ID used to identify BOX. This ID is unique for one function, DO NOT REUSE IT
	} box_t;
	static const MSPCommandHandler::box_t boxes[];
private:
	static const char * const flightControllerIdentifier;
	static const char * const BoardIdentifier;
	static const char * const TargetName ;
	static const char * const BoardName ;
	static const char * const Manufacturer ;

	static uint64_t m_activeBoxIds;

	// -----------------------
	static const uint8_t cMSP_HeaderLength = 4U;
	uint8_t m_MSP_HeadBuffer[cMSP_HeaderLength];
	uint8_t m_MSP_RxBufferIdx = 0U;
	uint8_t m_MSP_DataBuffer[64];
	typedef enum {
		eMSP_Header,
		eMSP_Data
	} MSP_State_t;
	MSP_State_t m_MSP_State = eMSP_Header;
	uint8_t m_MSP_Length = 0U;
	BigPipe<128, uint8_t> m_CommandPipe;
	BigPipe<384, uint8_t> m_ResponsePipe;


	static const char *pcHead;
	UART_HandleTypeDef *m_pUart;

	// response
	uint8_t m_MSP_RespBuffer[70];
	uint8_t *m_pWrtBuffer = m_MSP_RespBuffer;
	uint8_t m_WrtPtr = 0U;
	uint8_t m_XOR = 0U;

	uint16_t m_MSP_command_hcount = 0U;
	uint16_t m_MSP_command_count = 0U;
	uint16_t m_MSP_command_handled = 0U;
	uint16_t m_MSP_command_errors = 0U;

	uint8_t m_OSD_Switch = 0U;
	uint8_t m_SerialPassThroughStage = 0;
	uint8_t m_SerialResponseStage = 0;
	uint8_t m_SerialPassThroughLength = 0;
	MSPCommandHandler * m_pPassThroughPeer = nullptr;

	const FlightData_t *m_pFD = nullptr;

public:
	void startMsg(uint8_t Msgtype);
	void put8(uint8_t val);
	void put16(uint16_t val);
	void put32(uint32_t val);
	void putStr(const char * pStr);
	void putN(const uint8_t * pStr, const uint8_t Len);
	inline void finish();

	MSPCommandHandler(UART_HandleTypeDef *pUart = nullptr) : m_pUart(pUart), m_pPassThroughPeer(nullptr) {}
	void USBSend();
	int PrepResponse(uint8_t MSP_command);
	void ReceiveUsb(uint8_t *pBuffer, uint32_t Len);
	void ReceiveSerial(const uint8_t byte);
	bool SendSerial(uint8_t &byte);

	void transferBuffer(const uint8_t *pBuffer, const uint32_t Len);
	void stopPassthrough();

	void Init(const FlightData_t *pFD, MSPCommandHandler * pPassThroughPeer = nullptr);
	void Reset();
	void Cyclic();
	void setOSDLayout(uint8_t OSD_Switch);
	static uint64_t getActiveBoxIds() { return m_activeBoxIds;}

};

#endif /* OSD_MSPCOMMANDHANDLER_H_ */
