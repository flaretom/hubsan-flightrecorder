STM32F103 is used to sniff the SPI bus between transceiver module and MCU of the transmitter.
The FIFO access commands (0x05 - write, 0x45 - read) are analyzed and logged in different fileformats.
Furthermore the data is passed via MAVLink to smartphone-applications (like EZ-GUI, e.g. for a live map) 
and to a MinimOSD. The later can be used to get OSD on the external video output.

additional features:
* I2C for small OLED (settings menue or important data during flight) , or for status LEDs
* additional pins to connect buttons/switches (e.g. to set a marker in log file, settings-menue)
* software can be updated via SD-card



some additional info:

Pinout of A7105 module  
1   GND  
2   SS  
3   CLK  
4   SDIO  
5   GPIO?  
6   GPIO?  
7   GPIO?  
8   VCC  

Only pins 1..4 are required and connected to SPI1 of STM32F103.

(Result of https://gitlab.com/flaretom/hubsan-flightrecorder/issues/1)

Other ports used:
* SPI2 for SD-card
* USART1 (only RX) for GPS receive
* USART2 (output of serial data stream (MAVLink))
* USART3 (Bluetooth-module or serial connection to EZ-GUI for live map)
* USB (virtual comport to replace USART3 when connected to mobile phone(EZ-GUI))
 

 



