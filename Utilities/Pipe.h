
#ifndef Pipe_H
#define Pipe_H

#include <stdio.h>
#include <string.h>

#ifndef Min
  template <typename T> const T& Min (const T& a, const T& b) {
    return !(b<a)?a:b;     // or: return !comp(b,a)?a:b; for version (2)
  }
#endif

//## class Pipe
template <uint8_t cFIFOSize_u8, typename DT> class Pipe {
    ////    Operations    ////
    
public :

    //## operation Flush()
    void Flush();
    
    //## operation GetBufferSize()
    uint8_t GetBufferSize();
    
    //## operation GetFilled() const
    uint8_t GetFilled() const;
    
    //## operation GetFree() const
    uint8_t GetFree() const;
    
    //## operation Read(DT)
    bool Read(DT & DataOut_u8);
    
#if BLOCK_OPS
    //## operation Read(DT,u8)
    virtual u8 Read(DT* pData, const uint8_t Elements_u8);
#endif    
    //## operation Write(DT)
    bool Write(const DT& DataIn_u8);
    
#if BLOCK_OPS    
    //## operation Write(DT,u8)
    uint8_t Write(const DT * pData, const uint8_t Elements_u8);
#endif    
    ////    Attributes    ////

private :

    uint8_t m_ReadIndex_u8;		//## attribute m_ReadIndex_u8
    
    uint8_t m_WriteIndex_u8;		//## attribute m_WriteIndex_u8
    
    DT m_aByteBuffer[cFIFOSize_u8];		//## attribute m_aByteBuffer
};

//## class Pipe
template <uint8_t cFIFOSize_u8, typename DT> void Pipe<cFIFOSize_u8, DT>::Flush() {
    //#[ operation Flush()
    m_ReadIndex_u8=0U;
    m_WriteIndex_u8=0U;
    //#]
}

template <uint8_t cFIFOSize_u8, typename DT> uint8_t Pipe<cFIFOSize_u8, DT>::GetBufferSize() {
    //#[ operation GetBufferSize()
    return cFIFOSize_u8;
    //#]
}

template <uint8_t cFIFOSize_u8, typename DT> uint8_t Pipe<cFIFOSize_u8, DT>::GetFilled() const {
    //#[ operation GetFilled() const
    int16_t Delta_s16= m_WriteIndex_u8-m_ReadIndex_u8;
    if (0 > Delta_s16) {
    	Delta_s16+=cFIFOSize_u8;
    }
    return static_cast<uint8_t>(Delta_s16);
    //#]
}

template <uint8_t cFIFOSize_u8, typename DT> uint8_t Pipe<cFIFOSize_u8, DT>::GetFree() const {
    //#[ operation GetFree() const
    int16_t Delta_s16= m_ReadIndex_u8 - m_WriteIndex_u8 ;	// keep one element free to avoid wrap arround
    if (0 >= Delta_s16) {
    	Delta_s16+=cFIFOSize_u8;
    }
    return static_cast<uint8_t>(Delta_s16);
    //#]
}

template <uint8_t cFIFOSize_u8, typename DT> bool Pipe<cFIFOSize_u8, DT>::Read(DT & DataOut_u8) {
    //#[ operation Read(DT)
    bool Res=false;
    if (0U < GetFilled()) {
    	DataOut_u8=m_aByteBuffer[m_ReadIndex_u8];
        m_ReadIndex_u8++;
        if (cFIFOSize_u8 <= m_ReadIndex_u8) {
    		m_ReadIndex_u8=0U;
    	}
    	Res=true;
    }                   
    return Res;
    
    //#]
}

#if BLOCK_OPS
template <uint8_t cFIFOSize_u8, typename DT> uint8_t Pipe<cFIFOSize_u8, DT>::Read(DT* pData, const uint8_t Elements_u8) {
    //#[ operation Read(DT,u8)
    u8 RetElements_u8=0U;
    u8 ReadSize_u8 = Min(GetFilled(),Elements_u8);
    u8 part_u8=cFIFOSize_u8-m_ReadIndex_u8;  	// number of elements  from read-ptr until end of buffer
    if (ReadSize_u8 <= part_u8) {		// enough to complete Read in one pice?
    	memcpy(pData,&m_aByteBuffer[m_ReadIndex_u8],ReadSize_u8*sizeof(DT));
    	m_ReadIndex_u8+=ReadSize_u8;
    	RetElements_u8=ReadSize_u8;
    }
    else {
    	// read all data from read-ptr until end of buffer
    	memcpy(pData,&m_aByteBuffer[m_ReadIndex_u8],part_u8*sizeof(DT));
    	RetElements_u8=part_u8;
    	pData+=part_u8;
    	// read-ptr points now to begin of buffer
    
    	part_u8=ReadSize_u8-part_u8;	// number of remainig elements
    	if (m_WriteIndex_u8 < part_u8)  {  // number of require elements still in buffer (begin of buffer(0): write-ptr)
    		part_u8=m_WriteIndex_u8;		// number of elements still in buffer is smaller
    	}
    	memcpy(pData,&m_aByteBuffer,part_u8*sizeof(DT));	// copy elements
    	m_ReadIndex_u8=part_u8;		// move read-ptr behind last read element
    	RetElements_u8+=part_u8;
    }
    return RetElements_u8;
    //#]
}
#endif

template <uint8_t cFIFOSize_u8, typename DT> bool Pipe<cFIFOSize_u8, DT>::Write(const DT& DataIn_u8) {
    //#[ operation Write(DT)
    bool Res=false;
    if (0U < GetFree()) {
    	m_aByteBuffer[m_WriteIndex_u8]=DataIn_u8;
        m_WriteIndex_u8++;
        if (cFIFOSize_u8 <= m_WriteIndex_u8) {
    		m_WriteIndex_u8=0U;
    	}
    	Res=true;
    }                   
    return Res;
    
    //#]
}

#if BLOCK_OPS
template <uint8_t cFIFOSize_u8, typename DT> uint8_t Pipe<cFIFOSize_u8, DT>::Write(const DT * pData, const uint8_t Elements_u8) {
    //#[ operation Write(DT,u8)
    uint8_t part_u8=cFIFOSize_u8-m_WriteIndex_u8;  // number of elements  from write-ptr until end of buffer
    uint8_t Storable_u8= Min(GetFree(),Elements_u8);
    
    if (Storable_u8 < part_u8) {	// enough to complete Read in one piece? 
    								// 1byte + to avoid overlap of ReadPtr and WrtPtr
    	memcpy(&m_aByteBuffer[m_WriteIndex_u8],pData,Storable_u8*sizeof(DT));
    	m_WriteIndex_u8+=Storable_u8;
    } 
    else {       
    	// read all data from read-ptr until end of buffer
    	memcpy(&m_aByteBuffer[m_WriteIndex_u8],pData,part_u8*sizeof(DT));
    	pData+=part_u8;		
    	// write-ptr points now to begin of buffer	--> m_WriteIndex_u8=0
    
    	part_u8=Storable_u8-part_u8;	// number of remaining elements
    	if (0U < part_u8) {   // still something left to store and at least 1byte free?
    		memcpy(&m_aByteBuffer,pData,part_u8*sizeof(DT));	// copy elements
    	}
    	m_WriteIndex_u8=part_u8;		// move write-ptr behind last written element
    }
    return Storable_u8;
    //#]
}
#endif

#undef Min

#endif
