/*
 * HubsanDisplay.h
 *
 *  Created on: 17.01.2020
 *      Author: tom
 */

#ifndef HUBSANDISPLAY_H_
#define HUBSANDISPLAY_H_

#include <ssd1306.h>
#include <ssd1306_16bit.h>
#include <ssd1306_1bit.h>
#include <ssd1306_8bit.h>
#include <ssd1306_console.h>
#include <ssd1306_fonts.h>
#include <ssd1306_generic.h>
#include "nano_engine.h"

#include <Menu/IDisplayBase.h>

class HubsanDisplay : public IDisplayBase  {
	typedef struct {
		const uint8_t width;
		const uint8_t height;
		const uint8_t * const data;
	} BitmapList_t;
	static BitmapList_t BitmapList[2];
public:
	HubsanDisplay();
	virtual ~HubsanDisplay();

    virtual void printFixed( unsigned xpos, unsigned ypos, const char * const text
    		, IDisplayBase::IDisplayFlag_t flags = IDisplayBase::eNormal);
    virtual bool drawBitmap( unsigned xpos, unsigned ypos, int BitMapId, bool inverse = false);
    virtual void clear(bool clean);
    virtual void update();
    void initialize();
};

#endif /* HUBSANDISPLAY_H_ */
