/*
 * FlightData.cpp
 *
 *  Created on: 29.02.2020
 *      Author: tom
 */

#include "FlightData.h"
#include "Utilities/BasicTimer.h"
#include "Calendar.h"
#include <cstdio>
#include <math.h>
#include <algorithm>

FlightDataContainer FDC;		// singleton

int32_t FlightDataContainer::getI(const FlightDataIds item, bool &ok) const
{
	ok = true;
	switch (item) {
		case eDID_ELEVAT:		return FD.elevat;
		case eDID_SAT:			return FD.Sat;
		case eDID_H:			return FD.H;
		case eDID_R:			return FD.R;
		case eDID_P:			return FD.P;
		case eDID_THROTTLE:		return FD.throttle;
		case eDID_RUDDER:		return FD.rudder;
		case eDID_PITCH:		return FD.pitch;
		case eDID_YAW:			return FD.yaw;
		default :
			ok = false;
			return 0;
	}
}

uint32_t FlightDataContainer::getU(const FlightDataIds item, bool &ok) const
{
	ok = true;
	switch (item) {
		case eDID_TIME:			return 0;
		case eDID_DATE:			return 0;
		case eDID_LAT:			return FD.latit;
		case eDID_LONG:			return FD.longit;
		case eDID_DISTANCE:		return FD.distance;
		case eDID_VBAT:			return FD.VBat;
		case eDID_SAT:			return FD.Sat;
		case eDID_MARKER:		return FD.marker;
		case eDID_VIDEO:		return FD.video;
		case eDID_PHOTOTGL:		return FD.photoTgl;
		case eDID_AUX1:			return FD.Aux1;
		case eDID_AUX2:			return FD.Aux2;
		case eDID_RECORDTIME:	return (GetSysTime() - FD.RecordStart) / 1000;
		case eDID_BATPERCENT:	return EstimateBatCapacity(FD.VBat);
		case eDID_VELOCITY: 	return FD.velocity;
		case eDID_RSSI:			return FD.RSSI;
		case eDID_VALIDFRAMES:	return FD.validFrames;
		case eDID_GPS_MODE:	 	return FD.Aux2 & 0x04;
		case eDID_GPS_RTH:	 	return FD.Aux2 & 0x01;
		case eDID_MODE_I:		return ((0!=(FD.Aux2 & 0x04))?2:0) + ((0!=(FD.Aux2 & 0x01))?1:0);
		case eDID_VMAX:			return FD.vMax;
		default :
			ok = false;
			return 0;
	}
}

uint8_t FlightDataContainer::asString(const FlightDataIds item, char *szBuffer, const uint16_t bufsize) const
{
	const char *pIntFormat = "%i";
	const char *pHex4Format = "%04X";
	const char *pHex2Format = "%02X";
	const char *pInt10Format = "%li.%01li";
	const char *pDblFormat = "%li.%07li";
	const char *pBoolFormat ="%s";
	const int32_t cPrec = 10000000L;
	const int32_t cPrec10 = 10U;
	uint8_t len = 0U;
	bool ok = true;

	switch (item) {
	case eDID_TIME:
		len = snprintf(szBuffer, bufsize, "%s", m_TimeStr);		break;
	case eDID_DATE:
		len = snprintf(szBuffer, bufsize, "%s", m_DateStr);		break;
	case eDID_LAT:
	case eDID_LONG:
		len = snprintf(szBuffer, bufsize, pDblFormat, getU(item, ok)/cPrec, (getU(item, ok)%cPrec));	break;
	case eDID_ELEVAT:
		len = snprintf(szBuffer, bufsize, pInt10Format
				, getI(item, ok)/cPrec10, static_cast<uint32_t>(abs(getI(item, ok)%cPrec10))); 			break;
	case eDID_DISTANCE:
	case eDID_VBAT:
	case eDID_VELOCITY:
	case eDID_VMAX:
		len = snprintf(szBuffer, bufsize, pInt10Format
				, getU(item, ok)/cPrec10,  static_cast<uint32_t>(abs(getU(item, ok)%cPrec10))); 		break;
	case eDID_MODE_I:
	case eDID_SAT:
	case eDID_THROTTLE:
	case eDID_RUDDER:
	case eDID_PITCH:
	case eDID_YAW:
		len = snprintf(szBuffer, bufsize, pIntFormat, getI(item, ok));									break;
	case eDID_H:
	case eDID_R:
	case eDID_P:
		len = snprintf(szBuffer, bufsize, pInt10Format
				, getI(item, ok)/10,  static_cast<uint32_t>(abs(getI(item, ok)%10)));					break;
	case eDID_MARKER:
	case eDID_VIDEO:
	case eDID_PHOTOTGL:
	case eDID_RECORDTIME:
	case eDID_BATPERCENT:
	case eDID_RSSI:
	case eDID_VALIDFRAMES:
		len = snprintf(szBuffer, bufsize, pIntFormat, getU(item, ok));									break;
	case eDID_AUX1:
	case eDID_AUX2:
		len = snprintf(szBuffer, bufsize, pHex2Format, getU(item, ok));									break;
	case eDID_GPS_MODE:
	case eDID_GPS_RTH:
		len = snprintf(szBuffer, bufsize, pBoolFormat, getU(item, ok)?"On":"Off");						break;
	case eDID_MODE_T:
		len = snprintf(szBuffer, bufsize, pBoolFormat, (0!=(FD.Aux2 & 0x01))? "RTH": (0!=(FD.Aux2 & 0x04))? "GPS": "ATI");
		break;
	default :	break;
	}
	return len;
}

const char * FlightDataContainer::asString(const FlightDataIds item, uint8_t &length, const uint16_t bufsize) const
{
	static char s_szFieldBuffer[32];
	const uint16_t l = asString(item, s_szFieldBuffer, sizeof(s_szFieldBuffer));
	if (0U < bufsize) {
		length = std::min(bufsize, l);
		s_szFieldBuffer[length] = 0;
	}
	return s_szFieldBuffer;
}


void FlightDataContainer::startRecording()
{
	FD.RecordStart = GetSysTime();
	FD.vMax = 0;
}

void FlightDataContainer::updateTimeDate()
{
	Cal.getTimeStr(m_TimeStr, sizeof(m_TimeStr), ':');
	Cal.getDateStr(m_DateStr, sizeof(m_DateStr), '-');
}

void FlightDataContainer::HandleReceivedFrameE8(const uint8_t *framedata)
{
	// handle packages from A7105
	FD.Aux1 = framedata[9];	// bei start 0, dann 0x20
	FD.Aux2 = framedata[10];	// bei start 0, dann 0x07 ( aber nach Aux1 wechsel zu 0x20)
	FD.latit = (framedata[1] << 24) + (framedata[2] << 16) + (framedata[3] << 8) + (framedata[4] << 0);
	FD.longit = (framedata[5] << 24) + (framedata[6] << 16) + (framedata[7] << 8) + (framedata[8] << 0);
	//FD.distance = (framedata[13] << 24) + (framedata[14] << 16) + (framedata[15] << 8) + (framedata[16] << 0);
	FD.RSSI = framedata[14];
	FD.validFrames |= cNavData;
}
void FlightDataContainer::HandleReceivedFrameE7(const uint8_t *framedata)
{
	FD.VBat = framedata[13];
	FD.Sat = framedata[14];
	FD.velocity  = (framedata[7] << 8) + framedata[8];
	if (FD.velocity < FD.vMax) {
		FD.vMax = FD.velocity;
	}
	FD.H = (framedata[1] << 8) + framedata[2];
	FD.R = (framedata[3] << 8) + framedata[4];
	FD.P = (framedata[5] << 8) + framedata[6];
	FD.elevat = (framedata[11] << 8) + (framedata[12] << 0);
	FD.distance = (framedata[9] << 8) + (framedata[10] << 0);
	FD.validFrames |= cTeleData;
}

void FlightDataContainer::HandleReceivedFrame05(const uint8_t *framedata)
{
	static uint8_t photo = 0U;
	FD.throttle = 128 + framedata[2];
	FD.rudder = 128 + framedata[4];
	FD.pitch = 128 + framedata[6];
	FD.yaw = 128 + framedata[8];
	FD.video =  0U != (framedata[9] & 0x01);
	FD.photoTgl =  photo != (framedata[13] & 0x01);
	photo = framedata[13] & 0x01;
	FD.validFrames |= cControl;
}

void FlightDataContainer::InjectTestData(bool doInit)
{
	static int16_t heading = 0;
	static BasicTimer FotoTimer;
	static BasicTimer HeadingTimer;
	static BasicTimer PitchTimer;

	if (doInit) {
		FD.Aux1 = 1;	// bei start 0, dann 0x20
		FD.Aux2 = 2;	// bei start 0, dann 0x07 ( aber nach Aux1 wechsel zu 0x20)
		FD.latit = static_cast<uint32_t>(53.1234567891*10000000);
		FD.longit = static_cast<uint32_t>(8.2345678912*10000000);
		FD.elevat = 500;
		FD.distance = 0;
		FD.VBat = 82;
		FD.Sat = 9;
		FD.H = 0;
		FD.R = 0;
		FD.P = 0;
		FD.throttle = 128 + 1;
		FD.rudder = 128 + 2;
		FD.pitch = 128 + 3;
		FD.yaw = 128 + 4;
		FD.video =  0U;
		FotoTimer.SetPeriodicTimeout(300);
		HeadingTimer.SetPeriodicTimeout(10);
		PitchTimer.SetPeriodicTimeout(10);
	}
	else {
		FD.validFrames |= cNavData;
		FD.validFrames |= cTeleData;
		FD.validFrames |= cControl;
		if (FotoTimer.Timedout()) {
			FD.photoTgl =  1 - FD.photoTgl;
		}
		if (HeadingTimer.Timedout()) {
			++heading;
			if (360 <= heading) {
				heading = 0;
			}
			FD.H = heading;
		}
		if (PitchTimer.Timedout()) {
			++FD.distance;
			FD.pitch += 10;
			if (FD.pitch = 300) {
				FD.pitch = -300;
			}
		}
	}
}

bool FlightDataContainer::isStartStopCondition()
{
	bool StartStopDetected = false;
	if ( (-110 > FD.throttle) && (-110 > FD.rudder) && (110 < FD.pitch) && (-110 > FD.yaw)) {
		if (StartDetectTimer.Timedout()) {
			StartStopDetected = true;
			StartDetectTimer.SetSingleTimeout(1000);
		}
	}
	else {
		StartDetectTimer.SetSingleTimeout(1000);
	}
	return StartStopDetected;
}

bool FlightDataContainer::Right()
{
	return emulateButton( (-100 > FD.yaw) && (abs(FD.throttle)<10) && (abs(FD.rudder)<10), RepeatTimer[0U]);
}

bool FlightDataContainer::Left()
{
	return emulateButton((100 < FD.yaw) && (abs(FD.throttle)<10) && (abs(FD.rudder)<10), RepeatTimer[1U]);
}

bool FlightDataContainer::Up()
{
	return emulateButton((-100 > FD.pitch) && (abs(FD.throttle)<10) && (abs(FD.rudder)<10), RepeatTimer[2U]);
}

bool FlightDataContainer::Down()
{
	return emulateButton((100 < FD.pitch) && (abs(FD.throttle)<10) && (abs(FD.rudder)<10), RepeatTimer[3U]);
}

bool FlightDataContainer::emulateButton(bool pushed, BasicTimer &timer)
{
	bool pressed = false;
	if (pushed) {
		if (timer.IsActive()) {
			if (timer.Timedout()) {
				timer.SetSingleTimeout(200U);		// activate
				pressed = true;
			}
		}
		else {
			timer.SetSingleTimeout(400U);		// activate
			pressed = true;
		}
	}
	else {
		timer.SetPeriodicTimeout(0U);		// deactivate
	}
	return 	pressed;
}

uint8_t FlightDataContainer::EstimateBatCapacity(const uint8_t Vbat) const
{
	// https://www.rcgroups.com/forums/showpost.php?p=29431951
	uint16_t KapaVsVbat[][2] = {
		{100, 840},
		{95 , 830},
		{90 , 822},
		{85 , 816},
		{80 , 805},
		{75 , 797},
		{70 , 791},
		{65 , 783},
		{60 , 775},
		{55 , 771},
		{50 , 767},
		{45 , 763},
		{40 , 759},
		{35 , 757},
		{30 , 753},
		{25 , 749},
		{20 , 745},
		{15 , 741},
		{10 , 737},
		{ 5 , 722},
		{ 0 , 655},
		{ 0 , 0}
	};
	const uint16_t V = Vbat*10;
	uint8_t i;
	for (i = 0U; KapaVsVbat[i][1]; ++i) {
		if (KapaVsVbat[i][1] < V) {
			break;
		}
	}
	return KapaVsVbat[i][0];
	// better then (FD.VBat > 7.4)? (FD.VBat - 74) * 1000/(820U-740U) : 0U
}
