/*
 * DisplayBase.h
 *
 *  Created on: 17.01.2020
 *      Author: tom
 */

#ifndef IDISPLAYBASE_H_
#define IDISPLAYBASE_H_

class IDisplayBase {
public:
	typedef enum {
		eNormal,
		eTitle,
		eSelected,
		eMessage
	} IDisplayFlag_t;

    virtual void printFixed( unsigned xpos, unsigned ypos, const char * const text, IDisplayFlag_t flags = eNormal) = 0;
    virtual bool drawBitmap( unsigned xpos, unsigned ypos, int BitMapId, bool inverse = false) { return false;}
    virtual void clear(bool clean) = 0;
    virtual void update() = 0;
};

#endif /* IDISPLAYBASE_H_ */
