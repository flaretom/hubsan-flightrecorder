/*
 * FlRecConfig.cpp
 *
 *  Created on: 29.02.2020
 *      Author: tom
 */

#include <FlRecConfig/FlRecConfig.h>
#include <string.h>
#include <stdlib.h>

FlRecConfig GlobalSettings;		// singleton

FlRecConfig::FlRecConfig() {
	// TODO Auto-generated constructor stub
	strcpy(DisplFields[0], "@2,0Vbat:$7");
	strcpy(DisplFields[1], "@3,0Sat:$6");
	strcpy(DisplFields[2], "@3,2dt:$21");
	strcpy(DisplFields[3], "@4,0Aux1:$19");
	strcpy(DisplFields[4], "@4,2Aux2:$20");
	strcpy(DisplFields[5], "@5,0Dist:$6");
	strcpy(DisplFields[6], "@5,2Elev:$5");
	strcpy(DisplFields[7], "@6,0V:$23");
	strcpy(DisplFields[8], "@6,2RSSI:$24");
	strcpy(DisplFields[9], "@0,0Default screen");
}

uint8_t FlRecConfig::changeOSDScreen()
{
	++OsdChannel;
	if (2U < OsdChannel) {
		OsdChannel = 0U;
	}
	return OsdChannel;
}

const char *FlRecConfig::getDisplayFieldConfig(const uint8_t DisplayFieldNo) const
{
	if ((NoOfDisplayFields > DisplayFieldNo) && (0U != DisplFields[DisplayFieldNo][0])) {
		return DisplFields[DisplayFieldNo];
	}
	else {
		return nullptr;
	}
}

void FlRecConfig::findParameter(const char **pF)
{
	const char *p = *pF;
	while (*p && '=' != *p ) ++p;	// find =
	if ('=' == *p) {
		++p;	// skip over =
		while (*p && ' ' == *p ) ++p;	// skip trailing spaces
		*pF = p;
	}
}

void FlRecConfig::configureFiles(const char* parameters, bool &ok)
{
	const char * in = parameters;
	findParameter(&in);
	OutputFlags = OutputFlags_t::cNone;
	if (nullptr != strstr(in, "raw")) {
		OutputFlags = static_cast <OutputFlags_t> (OutputFlags  | OutputFlags_t::cRawFile);
	}
	if (nullptr != strstr(in, "gpx")) {
		OutputFlags = static_cast <OutputFlags_t> (OutputFlags | OutputFlags_t::cGPXFile);
	}
	if (nullptr != strstr(in, "txt")) {
		OutputFlags = static_cast <OutputFlags_t> (OutputFlags | OutputFlags_t::cTxtFile);
	}
	if (nullptr != strstr(in, "csv")) {
		OutputFlags = static_cast <OutputFlags_t> (OutputFlags | OutputFlags_t::cCsvFile);
	}
}

bool FlRecConfig::configureBool(const char* parameters, bool &ok)
{
	bool flag = false;
	const char * in = parameters;
	findParameter(&in);
	if (0 == strncmp(in, "true", 4U)) {
		flag = true;
	}
	else if (0 == strncmp(in, "false", 5U)) {
		flag = false;
	}
	else {
		ok = false;
	}
	return flag;
}

int32_t FlRecConfig::configureInt(const char* parameters, bool &ok)
{
	const char * in = parameters;
	findParameter(&in);
	return atoi(in);
}

bool FlRecConfig::configureString(const char* parameters, char *parameterStr, bool &ok)
{
	const char * in = parameters;
	while (*in && '\"' != *in ) ++in;	// find opening quote
	if ('\"' == *in) {		// quote found
		++in;
		const char *end = in ;
		while (*end && ('\"'!= *end)) {
			++end;
		}
		memcpy(parameterStr, in, end-in);
	}
	else {
		ok = false;
	}
}

bool FlRecConfig::isLineStartsWith(const char * line, const char * pStart)
{
	uint8_t tokenLen = strlen(pStart);
	bool fits = 0 == strncmp(line, pStart, tokenLen);
	return (fits && ((' '== line[tokenLen]) || ('='== line[tokenLen]))); // check if token ends with = or ' '
}

bool FlRecConfig::load(FIL *file)
{
	static const char* cCreatedFiles = "files";
	static const char* cCreatedGpx = "gpx";
	static const char* cCreatedCsv = "csv";
	static const char* cCreatedTxt = "txt";
	static const char* cCreatedRaw = "raw";
	static const char* cUpdateRate = "update";
	static const char* cOSDButton = "osdbutton";
	static const char* cOSDChannel = "osdchannel";
	static const char* cModeSwitch = "modeswitch";
	static const char* cUseJoystick = "joystick";
	static const char* cModeByJoystick = "modebyjoystick";
	static const char* cDisplayField = "disp_field";

	TCHAR lineBuffer[80];
	bool ok = true;
	uint8_t displayConfigIdx = 0U;
	OutputFlags = cNone;
	if (nullptr != file) {
		while (ok && !f_eof(file)) {
			char * in = f_gets(lineBuffer, sizeof(lineBuffer), file);
			if (nullptr != in) {
				while (*in && ' ' == *in ) ++in;	// skip trailing spaces
				if ('#' == *in ) {		// comment line
					continue;
				}
				// configure output files
				if (isLineStartsWith(in, cCreatedFiles)) {
					configureFiles(in, ok);
				}
				else if (isLineStartsWith(in, cCreatedGpx)) {
					OutputFlags = static_cast <OutputFlags_t> (OutputFlags | (configureBool(in, ok)? cGPXFile:cNone));
				}
				else if (isLineStartsWith(in, cCreatedCsv)) {
					OutputFlags = static_cast <OutputFlags_t> (OutputFlags | (configureBool(in, ok)? cCsvFile:cNone));
				}
				else if (isLineStartsWith(in, cCreatedTxt)) {
					OutputFlags = static_cast <OutputFlags_t> (OutputFlags | (configureBool(in, ok)? cTxtFile:cNone));
				}
				else if (isLineStartsWith(in, cCreatedRaw)) {
					OutputFlags = static_cast <OutputFlags_t> (OutputFlags | (configureBool(in, ok)? cRawFile:cNone));
				}

				// use of buttons or joystick
				else if (isLineStartsWith(in, cOSDButton)) {
					OSD_UseRedButton = configureBool(in, ok);
				}
				else if (isLineStartsWith(in, cModeSwitch)) {
					ModeIsSwitch = configureBool(in, ok);
				}
				else if (isLineStartsWith(in, cModeByJoystick)) {
					ModeByJoystick = configureBool(in, ok);
				}
				else if (isLineStartsWith(in, cUseJoystick)) {
					UseJoystick = configureBool(in, ok);
				}

				// all other settings
				else if (isLineStartsWith(in, cOSDChannel)) {
					OsdChannel = configureInt(in, ok);
				}
				else if (isLineStartsWith(in, cUpdateRate)) {
					UpdateRate = configureInt(in, ok);
				}
				else if (isLineStartsWith(in, cDisplayField)) {
					if (NoOfDisplayFields > displayConfigIdx) {
						if (configureString(in, DisplFields[displayConfigIdx], ok)) {
							++displayConfigIdx;
						}
					}
					else {
						ok = false;
					}
				}
				else {
					// tag unknown
				}
			}
		}
	}
	return ok;
}
