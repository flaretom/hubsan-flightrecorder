/*
 * TxtFileGen.h
 *
 *  Created on: 16.10.2019
 *      Author: tom
 */

#ifndef GPXFILEGEN_H_
#define GPXFILEGEN_H_

#include "IFileGen.h"
#include <ff.h>

class GpxFileGen : public IFileGen {
	FIL outfile;
	bool isEnabled = false;

	static const char *GpxfileHeader;
	static const char *GpxTrackname;
	static const char *GpxTrkpoint1;
	static const char *GpxTrkpoint2;
	static const char *GpxTrkpoint3;
	static const char *GpxTrkpoint4;
	static const char *GpxTrkpoint5;
	static const char *GpxTrkpointEnd;
	static const char *GpxfileFooter;
	static const char *GpxTrkpointInvalid;
public:
	GpxFileGen();
	virtual ~GpxFileGen();
	bool open(const char *date, const char* time);
	void close();
	bool createEntry(const FlightDataContainer &FDC);
};

#endif /* GPXFILEGEN_H_ */
